<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accountdeleted'] = '계정이 삭제되었습니다.';
$string['accountoptionsdesc'] = '여기서 계정에 대한 일반적인 옵션을 설정할 수 있습니다.';
$string['changepassworddesc'] = '만일 암호를 변경하고자 한다면 세부사항을 입력하세요';
$string['changepasswordotherinterface'] = '다른 인터페이스를 사용하여  <a href="%s">암호변경</a> 할 수 있습니다';
$string['changeusername'] = '새 사용자 이름';
$string['changeusernamedesc'] = '%s에 로그인하기 위한 사용자 이름. 사용자이름은 3-30문자로 공백을 제외한 숫자나 문자 및 기호를 포함할 수 있습니다.';
$string['changeusernameheading'] = '사용자 이름 변경';
$string['deleteaccount'] = '계정 삭제';
$string['deleteaccountdescription'] = '만일 계정이 삭제되면 신상정보 및 전시는 다른 사용자들에게 보여지지 않습니다. 포럼에 올린 글들은 남아있지만 저자의 이름은 표시되지 않습니다.';
$string['friendsauth'] = '새 친구가 될려면 내 승인이 필요';
$string['friendsauto'] = '새 친구 자동 승인';
$string['friendsdescr'] = '친구 통제';
$string['friendsnobody'] = '아무도 나를 친구로 추가할 수 없음';
$string['language'] = '언어';
$string['messagesallow'] = '누구든지 나에게 메세지 보내기 허용';
$string['messagesdescr'] = '다른 사용자에게서 온 메세지';
$string['messagesfriends'] = '친구목록에 있는 사람이 나에게 메세기 보내기 허용';
$string['messagesnobody'] = '다른 사람이 나에게 메세지 보내기 불허';
$string['off'] = '꺼짐';
$string['oldpasswordincorrect'] = '현재의 암호가 아닙니다.';
$string['on'] = '켜짐';
$string['prefsnotsaved'] = '맞춤설정 저장 실패!';
$string['prefssaved'] = '맞춤설정이 저장됨';
$string['showviewcolumns'] = '전시를 편집할때 열을 추가하거나 제거하기 위한 컨트롤 표시';
$string['tagssideblockmaxtags'] = '태그구름에서 최대 태그수';
$string['tagssideblockmaxtagsdescription'] = '태그 구름에서 표시될 최대 태그 수';
$string['updatedfriendcontrolsetting'] = '친구 통제를 갱신하였습니다.';
$string['wysiwygdescr'] = 'HTML 에디터';
?>
