<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['About'] = '정보';
$string['Admin'] = '관리자';
$string['Created'] = '생성됨';
$string['Files'] = '파일';
$string['Friends'] = '친구들';
$string['Group'] = '모둠';
$string['Joined'] = '가입함';
$string['Members'] = '구성원';
$string['Reply'] = '답장';
$string['Role'] = '역할';
$string['Views'] = '보여주기';
$string['acceptinvitegroup'] = '승인';
$string['addedtofriendslistmessage'] = '%s가 친구로 추가되었습니다. %s는 이제 친구목록에 올라와 있습니다. 친구들의 개인정보페이지를 보기 위해서는 아래 링크를 클릭하세요.';
$string['addedtofriendslistsubject'] = '새 친구';
$string['addedtogroupmessage'] = '%s가 모둠 \'%s\'에 추가되었습니다. 모둠을 보기 위해서는 아래 링크를 클릭하세요.';
$string['addedtogroupsubject'] = '모둠에 추가되었습니다.';
$string['addnewinteraction'] = '새 %s 추가';
$string['addtofriendslist'] = '친구에 추가';
$string['addtomyfriends'] = '내 친구로 추가';
$string['adduserfailed'] = '사용자 추가 실패';
$string['addusertogroup'] = '추가됨';
$string['allfriends'] = '모든 친구들';
$string['allgroupmembers'] = '모든 모둠 구성원';
$string['allgroups'] = '모든 모둠';
$string['allmygroups'] = '모든 내 모둠';
$string['approve'] = '승인';
$string['approverequest'] = '요청 승인!';
$string['backtofriendslist'] = '친구목록으로 돌아가기';
$string['cannotinvitetogroup'] = '이 모둠에 이 사용자를 초청할 수 없습니다.';
$string['cannotrequestfriendshipwithself'] = '당신을 당신 친구로 초청할 수 없습니다.';
$string['cannotrequestjoingroup'] = '이 모둠에 가입 요청할 수 없습니다.';
$string['cantdeletegroup'] = '이 모둠을 삭제할 수 없습니다.';
$string['cantdenyrequest'] = '유효한 친구관계를 요청이 아닙니다.';
$string['canteditdontown'] = '모둠을 소유하고 있지 않아서 이 모둠을 편집할 수 없습니다.';
$string['cantleavegroup'] = '이 모둠을 탈퇴할 수 없습니다.';
$string['cantmessageuser'] = '이 사용자에게 메세지를 보낼 수 없습니다.';
$string['cantremovefriend'] = '친구 목록에서 이 사용자를 제거할 수 없습니다.';
$string['cantrequestfriendship'] = '이 사용자에게 친구관계를 요청할 수 없습니다.';
$string['cantrequestfrienship'] = '이 사용자로부터 친구관계를 요청할 수 없습니다.';
$string['cantviewmessage'] = '이 메세지를 볼 수 없습니다.';
$string['changerole'] = '역할 변경';
$string['changeroleofuseringroup'] = '%s (%s)의 역할 변경';
$string['changeroleto'] = '역할 변경';
$string['confirmremovefriend'] = '친구목록에서 이 사용자를 제거하기를 원하십니까?';
$string['couldnotjoingroup'] = '이 모둠에 가입할 수 없습니다.';
$string['couldnotleavegroup'] = '이 모둠에서 탈퇴할 수 없습니다';
$string['couldnotrequestgroup'] = '모둠 구성원자격 요청을 보낼 수 없습니다.';
$string['creategroup'] = '모둠 만들기';
$string['currentfriends'] = '현재 친구';
$string['currentrole'] = '현재역할';
$string['declineinvitegroup'] = '거절';
$string['declinerequest'] = '요청 거절';
$string['deletegroup'] = '모둠이 성공적으로 삭제되었습니다.';
$string['deleteinteraction'] = '%s \'%s\' 삭제';
$string['deleteinteractionsure'] = '이것을 하기를 원하십니까? 되돌릴 수 없습니다.';
$string['deletespecifiedgroup'] = '모둠 \'%s\' 삭제';
$string['denyfriendrequest'] = '친구 요청 거절';
$string['denyfriendrequestlower'] = '친구 요청 거절';
$string['denyrequest'] = '요청 거절';
$string['editgroup'] = '모둠 편집';
$string['existingfriend'] = '기존 친구';
$string['findnewfriends'] = '새 친구 찾기';
$string['friend'] = '친구';
$string['friendformacceptsuccess'] = '친구 요청 수락';
$string['friendformaddsuccess'] = '%s를 친구 목록에 추가';
$string['friendformrejectsuccess'] = '친구 요청 거절함';
$string['friendformremovesuccess'] = '%s를 친구 목록에서 제거';
$string['friendformrequestsuccess'] = '%s에게 친구요청 보냄';
$string['friendlistfailure'] = '친구목록 변경 실패';
$string['friendrequestacceptedmessage'] = '%s가 친구요청을 승인하였으며 당신의 친구 목록에 추가되되었습니다.';
$string['friendrequestacceptedsubject'] = '친구요청이 승인됨';
$string['friendrequestrejectedmessage'] = '%s가 친구요청을 거절하였습니다.';
$string['friendrequestrejectedmessagereason'] = '%s가 친구요청을 거절하였습니다. 이유는 다음과 같습니다.';
$string['friendrequestrejectedsubject'] = '친구요청이 거절됨';
$string['friends'] = '친구들';
$string['friendshipalreadyrequested'] = '%s의 친구 목록에 추가되기를 요청하였습니다.';
$string['friendshipalreadyrequestedowner'] = '%s가 당신의 친구 목록에 추가되기를 요청하였습니다.';
$string['friendshiprequested'] = '친구 요청됨';
$string['group'] = '모둠';
$string['groupadmins'] = '모둠 관리자';
$string['groupalreadyexists'] = '이 이름의 모둠이 이미 존재합니다.';
$string['groupconfirmdelete'] = '이 모둠을 삭제하기를 희망하십니까?';
$string['groupconfirmdeletehasviews'] = '이 모둠을 삭제하는 것이 확실합니까? 보여주기 중 몇개는 접근제어를 위해 이 모둠을 사용하는데, 이 모둠을 제거하면 모둠의 구성원들이 보여주기를 접근할 수 없습니다.';
$string['groupconfirmleave'] = '이 모둠을 탈퇴하기를 원하십니까?';
$string['groupconfirmleavehasviews'] = '이 모둠을 탈퇴하는 것이 확실합니까? 보여주기 중 몇개는 접근제어를 위해 이 모둠을 사용하는데, 이 모둠을 탈퇴하면 모둠의 구성원들이 보여주기를 접근할 수 없습니다.';
$string['groupdescription'] = '모둠 설명';
$string['grouphaveinvite'] = '이 모둠에 가입하도록 초청받았습니다.';
$string['grouphaveinvitewithrole'] = '이 모둠에 다음 역할로 가입하도록 초청받았습니다.';
$string['groupinteractions'] = '모둠 활동';
$string['groupinviteaccepted'] = '초청이 성공적으로 승인되었습니다. 이제 모둠의 구성원입니다.';
$string['groupinvitedeclined'] = '초청이 성공적으로 거절되었습니다.';
$string['groupinvitesfrom'] = '초청 가입';
$string['groupjointypecontrolled'] = '이 모둠에 가입은 통제됩니다. 이 모둠에 가입할 수 없습니다.';
$string['groupjointypeinvite'] = '이 모둠에 가입은 초청에 의해서만 가능';
$string['groupjointypeopen'] = '이 모둠 가입은 열려있습니다. 자유롭게 가입하세요.';
$string['groupjointyperequest'] = '이 모둠에 가입은 요청에 의해서만 가능';
$string['groupmemberrequests'] = '보류중인 구성원자격 요청';
$string['groupmembershipchangedmessageaddedmember'] = '이 모둠의 구성원으로 추가되었습니다.';
$string['groupmembershipchangedmessageaddedtutor'] = '이 모둠의 튜터로 추가되었습니다.';
$string['groupmembershipchangedmessagedeclinerequest'] = '이 모둠 가입요청이 거절되었습니다.';
$string['groupmembershipchangedmessagemember'] = '이 모둠의 튜터에서 일반 구성원으로 변경되었습니다.';
$string['groupmembershipchangedmessageremove'] = '이 모둠에서 탈퇴되었습니다.';
$string['groupmembershipchangedmessagetutor'] = '이 모둠의 튜터로 승격되었습니다.';
$string['groupmembershipchangesubject'] = '모둠 구성원자격:%s';
$string['groupname'] = '모둠 이름';
$string['groupnotfound'] = '아이디가 %s인 모둠이 없습니다.';
$string['groupnotinvited'] = '이 모둠에 가입 초청을 받지 않았습니다.';
$string['grouprequestmessage'] = '%s가 모둠 %s에 가입을 희망 합니다.';
$string['grouprequestmessagereason'] = '%s가 모둠 %s에 가입을 희망 합니다. 가입 사유: %s';
$string['grouprequestsent'] = '모둠 구성원 자격 요청이 보내어졌습니다.';
$string['grouprequestsubject'] = '새 모둠 구성원자격 요청';
$string['groups'] = '모둠';
$string['groupsaved'] = '모둠이 성공적으로 저장되었습니다.';
$string['groupsimin'] = '내가 속한 모둠';
$string['groupsiminvitedto'] = '초청받은 모둠';
$string['groupsiown'] = '내가 주인인 모둠';
$string['groupsiwanttojoin'] = '가입희망 모둠';
$string['groupsnotin'] = '소속되지 않은 모둠';
$string['grouptype'] = '모둠 타입';
$string['hasbeeninvitedtojoin'] = '이 모둠에 가입하도록 초청 받았습니다.';
$string['hasrequestedmembership'] = '이 모둠의 구성원 자격을 요청하였습니다.';
$string['interactiondeleted'] = '%s가 성공적으로 삭제되었습니다.';
$string['interactionsaved'] = '%s가 성공적으로 저장되었습니다.';
$string['invalidgroup'] = '모둠이 없습니다.';
$string['invite'] = '초청';
$string['invitemembertogroup'] = '%s를 \'%s\'에 가입하도록 초청';
$string['invitetogroupmessage'] = '%s가 당신을 모둠\'%s\'에 가입하도록 초청하였습니다. 자세한 정보는 다음 링크를 클릭하세요.';
$string['invitetogroupsubject'] = '모둠에 가입하도록 초청받았습니다.';
$string['inviteuserfailed'] = '사용자 초청 실패';
$string['inviteusertojoingroup'] = '초청:';
$string['joinedgroup'] = '이제 모둠 구성원입니다.';
$string['joingroup'] = '이 모둠에 가입하기';
$string['leavegroup'] = '이 모둠을 탈퇴하기';
$string['leavespecifiedgroup'] = '%s 모둠 탈퇴';
$string['leftgroup'] = '이 모둠을 탈퇴합니다.';
$string['leftgroupfailed'] = '모둠 탈퇴 실패';
$string['member'] = '구성원';
$string['memberchangefailed'] = '구성원자격 정보 갱신 실패';
$string['memberchangesuccess'] = '구성원자격이 성공적으로 변경되었습니다.';
$string['memberrequests'] = '구성원자격 요청';
$string['members'] = '구성원';
$string['membershiprequests'] = '구성원자격 요청';
$string['membershiptype'] = '모둠 구성원 자격 형식';
$string['membershiptype.controlled'] = '통제되는 구성원 자격';
$string['membershiptype.invite'] = '초청만 가능';
$string['membershiptype.open'] = '공개된 구성원자격';
$string['membershiptype.request'] = '구성원자격 요청';
$string['memberslist'] = '구성원:';
$string['messagebody'] = '메세지 보내기';
$string['messagenotsent'] = '메세지 보내기 실패';
$string['messagesent'] = '메세지를 보냈습니다.';
$string['newusermessage'] = '%s 로부터 새 메세지';
$string['newusermessageemailbody'] = '%s가 메세지를 보냈습니다. 이 메세지를 보려면 %s를 방문하세요.';
$string['nobodyawaitsfriendapproval'] = '친구가 되기 위해 승인을 기다리는 사람이 없습니다.';
$string['nogroups'] = '모둠이 없음';
$string['nogroupsfound'] = '발견된 모둠이 없습니다:(';
$string['nointeractions'] = '모둠에 활동이 없습니다.';
$string['nosearchresultsfound'] = '검색 결과가 없습니다:(';
$string['notallowedtodeleteinteractions'] = '이 모둠에서 활동을 삭제할 권한이 없습니다.';
$string['notallowedtoeditinteractions'] = '이 모둠에서 활동을 추가하거나 편집할 권한이 없습니다.';
$string['notamember'] = '이 모둠의 구성원이 아닙니다.';
$string['notinanygroups'] = '아무 모둠에도 속하지 않음';
$string['notmembermayjoin'] = '이 페이지를 보기위해서는 모둠 %s에 가입해야 합니다.';
$string['noviewstosee'] = '볼 수 있는 것이 없습니다. :(';
$string['pending'] = '보류중';
$string['pendingfriends'] = '보류중인 친구들';
$string['pendingmembers'] = '보류중인 구성원';
$string['publiclyviewablegroup'] = '공개적으로 볼수 있는 모둠';
$string['publiclyviewablegroupdescription'] = '모든 사람에게 이 모둠을 볼수 있는 권한 허용(이사이트에 등록하지 않은 사람 포함)';
$string['reason'] = '이유';
$string['reasonoptional'] = '이유(선택사항)';
$string['reject'] = '거절';
$string['rejectfriendshipreason'] = '요청 거절 사유';
$string['releaseview'] = '보여주기 공개';
$string['remove'] = '제거';
$string['removedfromfriendslistmessage'] = '%s가 친구목록에서 제거되었습니다.';
$string['removedfromfriendslistmessagereason'] = '%s가 친구목록에서 제거되었습니다. 사유:';
$string['removedfromfriendslistsubject'] = '친구 목록에서 제거됨';
$string['removefriend'] = '친구 제거';
$string['removefromfriends'] = '친구들에서 %s를 제거';
$string['removefromfriendslist'] = '친구들에서 제거';
$string['removefromgroup'] = '모둠에서 제거';
$string['request'] = '요청';
$string['requestedfriendlistmessage'] = '%s는 당신이 그들을 친구로 추가할 것을 요청하였습니다. 아래 링크를 사용하여 추가하거나 친구 목록페이지에서 추가할 수 있습니다.';
$string['requestedfriendlistmessagereason'] = '%s는 당신이 그들을 친구로 추가할 것을 요청하였습니다. 아래 링크를 사용하여 추가하거나 친구 목록페이지에서 추가할 수 있습니다. 사유:';
$string['requestedfriendlistsubject'] = '새 친구 요청';
$string['requestedfriendship'] = '친구 요청';
$string['requestedmembershipin'] = '구성원자격 요청함';
$string['requestedtojoin'] = '이 모둠에 가입을 요청하였습니다.';
$string['requestfriendship'] = '친구 요청';
$string['requestjoingroup'] = '이 모둠 가입 요청';
$string['requestjoinspecifiedgroup'] = '모둠 \'%s\'에 가입 요청';
$string['rolechanged'] = '역할 변경됨';
$string['savegroup'] = '모둠 저장';
$string['seeallviews'] = '모든 %s 보여주기 보기';
$string['sendfriendrequest'] = '친구 요청 보냄';
$string['sendfriendshiprequest'] = '%s에게 친구 요청 보냄';
$string['sendinvitation'] = '초대 보냄';
$string['sendmessage'] = '메세지 보냄';
$string['sendmessageto'] = '%s에게 메세지 보냄';
$string['submittedviews'] = '제출된 보여주기';
$string['title'] = '제목';
$string['trysearchingforfriends'] = '네트워크를 확장하기 위해 %s 검색을 시도하여 새 친구 %s 를 찾으세요.';
$string['trysearchingforgroups'] = '%s 검색 해서 가입할 모둠 %s을 찾아보세요.';
$string['updatemembership'] = '구성원자격 업데이트';
$string['user'] = '사용자';
$string['useradded'] = '사용자 추가됨';
$string['useralreadyinvitedtogroup'] = '이 사용자는 이미 초청받았거나, 이미 이 모둠의 구성원입니다.';
$string['usercannotchangetothisrole'] = '사용자가 이 역할로 변경될 수 없습니다.';
$string['usercantleavegroup'] = '사용자는 이 모둠을 탈퇴할 수 없습니다.';
$string['userdoesntwantfriends'] = '사용자는 새로운 친구들을 원하지 않습니다.';
$string['userinvited'] = '초대 보냄';
$string['userremoved'] = '사용자 제거됨';
$string['users'] = '사용자';
$string['usersautoadded'] = '사용자 자동 추가?';
$string['usersautoaddeddescription'] = '모든 새 사용자를 이 모둠에 자동적으로 포함시킬까요?';
$string['viewmessage'] = '메세지 보기';
$string['viewreleasedmessage'] = '%s에 제출된 보여주기가 %s에 의해 당신에게 되돌아 왔습니다.';
$string['viewreleasedsubject'] = '보여주기가 공개되었습니다.';
$string['viewreleasedsuccess'] = '보여주기가 성공적으로 공개되었습니다.';
$string['whymakemeyourfriend'] = '나를 친구로 하면 좋은 이유입니다.';
$string['youaregroupmember'] = '이 모둠의 구성원입니다.';
$string['youowngroup'] = '이 모둠을 소유하고 있습니다.';
?>
