<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = '매우 큰 가운데 컬럼 15,70,15';
$string['20,30,30,20'] = '큰 가운데 컬럼 20,30,30,20';
$string['25,25,25,25'] = '같은 폭 25,25,25,25';
$string['25,50,25'] = '가운데 큰 컬럼
25,50,25';
$string['33,33,33'] = '같은 폭
33,33,33';
$string['33,67'] = '큰 오른쪽 컬럼 33,67';
$string['50,50'] = '같은 폭 50,50';
$string['67,33'] = '큰 왼쪽 컬럼
67,33';
$string['Added'] = '추가됨';
$string['Browse'] = '브라우즈';
$string['Configure'] = '설정';
$string['Owner'] = '소유자';
$string['Preview'] = '미리보기';
$string['Search'] = '검색';
$string['Template'] = '템플릿';
$string['Untitled'] = '제목 없음';
$string['View'] = '보여주기';
$string['Views'] = '보여주기';
$string['access'] = '접근';
$string['accessbetweendates2'] = '다른 사람은 %s일 이전이나 %s 이후에 이 보여주기를 볼 수 없습니다.';
$string['accessfromdate2'] = '다른 사람은 %s일 이전에 이 보여주기를 볼 수 없습니다.';
$string['accessuntildate2'] = '다른 사람은 %s일 이후에 이 보여주기를 볼 수 없습니다.';
$string['add'] = '추가';
$string['addcolumn'] = '컬럼 추가';
$string['addedtowatchlist'] = '이 보여주기가 관심목록에 추가되었습니다.';
$string['addfeedbackfailed'] = '피드백 추가 실패';
$string['addnewblockhere'] = '여기에 새 블록 추가';
$string['addtowatchlist'] = '관심목록에 보여주기 추가';
$string['addtutors'] = '튜터 추가';
$string['addusertogroup'] = '모둠에 이 사용자 추가';
$string['allowcopying'] = '복사 허용';
$string['allviews'] = '모든 보여주기';
$string['alreadyinwatchlist'] = '이 보여주기는 이미 관심목록에 있습니다.';
$string['artefacts'] = '작품';
$string['artefactsinthisview'] = '이 보여주기안의 작품';
$string['attachedfileaddedtofolder'] = '첨부된 파일 %s이 \'%s\' 폴더에 추가되었습니다.';
$string['attachfile'] = '파일 첨부';
$string['attachment'] = '첨부';
$string['back'] = '뒤로';
$string['backtocreatemyview'] = '내 보여주기 만들기로 돌아가기';
$string['backtoyourview'] = '내 보여주기로 돌아가기';
$string['blockcopypermission'] = '복사 허용 차단';
$string['blockcopypermissiondesc'] = '다른 사용자에게 보여주기 복사를 허용하는 경우 블록 복사 방법을 선택할 수 있습니다.';
$string['blockinstanceconfiguredsuccessfully'] = '블록이 성공적으로 설정되었습니다.';
$string['blocksinstructionajax'] = '보여주기 레이아웃에 블록을 추가하기 위해 이 선 아래로 드래그 하세요. 블록을 드래그하여 원하는 위치에 놓을 수 있습니다.';
$string['blocksintructionnoajax'] = '블록을 선택하고 보여주기의 어느 곳에 추가할지를 선택하세요. 블록의 제목막대에 있는 화살표를 사용하여 블록의 위치를 정할 수 있습니다.';
$string['blocktitle'] = '블록 제목';
$string['blocktypecategory.feeds'] = '외부 피드';
$string['blocktypecategory.fileimagevideo'] = '파일, 이미지, 비디오';
$string['blocktypecategory.general'] = '일반';
$string['by'] = ':';
$string['cantdeleteview'] = '이 보여주기를 삭제할 수 없습니다.';
$string['canteditdontown'] = '소유권이 없으므로 편집할 수 없습니다';
$string['canteditdontownfeedback'] = '소유권이 없으므로 이 피드백을 편집할 수 없습니다';
$string['canteditsubmitted'] = '보여주기가 "%s"로부터 평가받기 위해 제출되어 편집할 수 없습니다. 튜터가 보여주기를 되돌려 줄 때 까지 기다려야 합니다.';
$string['cantsubmitviewtogroup'] = '평가를 위해 이 모둠에게 보여주기를 제출할 수 없습니다.';
$string['category.academic'] = '학문적 템플릿';
$string['category.all'] = '모든 템플릿';
$string['category.blog'] = '블로그 템플릿';
$string['category.gallery'] = '갤러리 템플릿';
$string['category.personaldevelopment'] = '개인 개발 템플릿';
$string['category.professionaldevelopment'] = '전문 개발 템플릿';
$string['category.resume'] = '이력서 템플렛';
$string['changemyviewlayout'] = '내 보여주기 레이아웃 변경';
$string['changeviewlayout'] = '내 보여주기 레이아웃 변경';
$string['chooseformat'] = '이 블록에 작품들이 어떻게 표시될지를 선택하세요 ...';
$string['choosetemplategrouppagedescription'] = '<p>이 모둠에게 복사가 허용된 보여주기를 검색해서 새 보여주기를 만드는 출발점으로 삼을 수 있습니다. 이름을 클릭하여 보여주기를 미리보기 한세요. 복사하기를 원하는 보여주기를 찾으면 복사하기 위해 "보여주기 복사"를 클릭한 다음 그것을 고쳐보도록 하세요.</p>
<p><strong>참고:</strong> 모둠은 현재 블로그나 블로그 게시글을 복사할 수 없습니다..</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>이 기관에게 복사가 허용된 보여주기를 검색해서 새 보여주기를 만드는 출발점으로 삼을 수 있습니다. 이름을 클릭하여 보여주기를 미리보기 한세요. 복사하기를 원하는 보여주기를 찾으면 복사하기 위해 "보여주기 복사"를 클릭한 다음 그것을 고쳐보도록 하세요.</p>
<p><strong>참고:</strong> 기관은 현재 블로그나 블로그 게시글을 복사할 수 없습니다..</p>';
$string['choosetemplatepagedescription'] = '<p>복사가 허용된 보여주기를 검색해서 새 보여주기를 만드는 출발점으로 삼을 수 있습니다. 이름을 클릭하여 보여주기를 미리보기 한세요. 복사하기를 원하는 보여주기를 찾으면 복사하기 위해 "보여주기 복사"를 클릭한 다음 그것을 고쳐보도록 하세요.</p>';
$string['clickformoreinformation'] = '추가정보와 피드백을 하기 위해 클릭하세요.';
$string['comment'] = '댓글';
$string['comments'] = '댓글';
$string['complaint'] = '불만';
$string['configureblock'] = '%s 블록 설정';
$string['configurethisblock'] = '이 블록 설정';
$string['confirmcancelcreatingview'] = '이 보여주기는 완성되지 않았습니다. 취소하시겠습니까?';
$string['confirmdeleteblockinstance'] = '이 블록을 삭제하기를 원하십니까?';
$string['copiedblocksandartefactsfromtemplate'] = '%s 에서 %d의 블록과 %d의 작품을 복사했습니다.';
$string['copyaview'] = '보여주기 복사';
$string['copyfornewgroups'] = '새 모둠을 위한 복사';
$string['copyfornewgroupsdescription'] = '이 모둠 형태의 모든 새 모둠에서 이 보여주기 복사';
$string['copyfornewmembers'] = '새 기관 구성원을 위한 복사';
$string['copyfornewmembersdescription'] = '%s의 모든 새 구성원들을 위해 이 보여주기를 자동 복사함.';
$string['copyfornewusers'] = '새 사용자를 위한 복사';
$string['copyfornewusersdescription'] = '사용자가 만들어질때 사용자의 이포트폴리오에 이 보여주기를 자동복사';
$string['copynewusergroupneedsloggedinaccess'] = '새 사용자나 모둠을 위해 복사된 보여주기는 로그인 사용자들이 접근할 수 있어야 합니다.';
$string['copythisview'] = '이 보여주기 복사';
$string['copyview'] = '보여주기 복사';
$string['createemptyview'] = '빈 보여주기 만들기';
$string['createtemplate'] = '템플릿 만들기';
$string['createview'] = '보여주기 만들기';
$string['createviewstep1'] = '전시 만들기 - 1/4단계';
$string['createviewstep2'] = '전시 만들기 - 2/4단계';
$string['createviewstep3'] = '전시 만들기 - 3/4단계';
$string['createviewstep4'] = '전시 만들기 - 4/4단계';
$string['createviewstepone'] = '보여주기 만들기 1단계: 레이아웃';
$string['createviewstepthree'] = '보여주기 만들기 3단계: 접근권한';
$string['createviewsteptwo'] = '보여주기 만들기 2단계: 세부사항';
$string['date'] = '일자';
$string['deletespecifiedview'] = '보여주기 "%s" 삭제';
$string['deletethisview'] = '이 보여주기 삭제';
$string['deleteviewconfirm'] = '이 보여주기를 삭제하기를 원하십니까? 되돌릴 수 없습니다.';
$string['description'] = '보여주기 설명';
$string['displaymyview'] = '내 보여주기 표시';
$string['editaccess'] = '전시 접근 편집';
$string['editaccessforview'] = '보여주기 "%s"에 대한 접근 권한 편집';
$string['editaccesspagedescription2'] = '<p>기본적으로 보여주기는 자신만 볼 수 있습니다. 여기에서 보여주기를 볼 수 있는 사람들을 선택할 수 있습니다. 추가를 클릭해서 공개하거나 로그인 사용자, 친구들에서 볼 수 있는 권한을 주세요. 검색창을 이용해서 개인이나 모둠을 추가할 수 있습니다. 추가한 사용자들은 오른쪽 창에 보여질 것입니다.
</p><p>당신의 보여주기를 자신들의 이포트폴리오에 복사할 수 있는 권한을 다른 사용자들에게 줄 수 있습니다. 사용자가 보여주기를 복사하면 보여주기에 있는 모든 파일과 폴더 사본을 갖게 됩니다.</p> <p> 일단 하였으면 아래로 스크롤하여 저장바튼을 클릭하세요.';
$string['editblocksforview'] = '보여주기 "%s" 편집';
$string['editblockspagedescription'] = '<p>보여주기에 어떤 블록을 표시할 수 있는지 알아보기 위해 아래의 탭에서 선택하세요. 블록들을 드래그 드롭하여 보여주기 레이아웃에 가져다 놓을 수 있습니다. 자세한 정보를 위해서는 ? 아이콘을 선택하세요.</p>';
$string['editmetadata'] = '전시 정보 편집';
$string['editmyview'] = '내 보여주기 편집';
$string['editprofileview'] = '프로파일 보여주기 편집';
$string['editthisview'] = '이 보여주기 편집';
$string['editview'] = '전시 편집';
$string['editviewaccess'] = '보여주기 접근권한 편집';
$string['editviewdetails'] = '보여주기 "%s"의 세부사항 편집';
$string['editviewnameanddescription'] = '보여주기 세부사항 편집';
$string['empty_block'] = '왼쪽에 있는 트리로부터 작품을 선택하여 여기에 놓으세요';
$string['emptylabel'] = '이 라벨에 대한 문장을 입력하기 위해 여기를 클릭하세요';
$string['err.addblocktype'] = '보여주기에 블록을 추가하지 못하였습니다.';
$string['err.addcolumn'] = '새 열을 추가하는데 실패하였습니다.';
$string['err.moveblockinstance'] = '지정된 곳으로 블록을 이동하지 못 하였습니다.';
$string['err.removeblockinstance'] = '블록을 삭제할 수 없습니다.';
$string['err.removecolumn'] = '열 삭제 실패';
$string['everyoneingroup'] = '모둠내 모두';
$string['feedback'] = '피드백';
$string['feedbackattachdirdesc'] = '보여주기 평가에 첨부된 파일들';
$string['feedbackattachdirname'] = '평가 파일들';
$string['feedbackattachmessage'] = '첨부 파일이 %s 폴더에 추가되었습니다.';
$string['feedbackchangedtoprivate'] = '피드백이 비공개로 변경되었습니다';
$string['feedbackonthisartefactwillbeprivate'] = '이 작품에 대한 피드백은 소유자만 볼 수 있습니다.';
$string['feedbackonviewbytutorofgroup'] = '%s에 대한 피드백 : %s 소속: %s';
$string['feedbacksubmitted'] = '피드백이 제출됨';
$string['filescopiedfromviewtemplate'] = '%s로 부터 파일이 복사됨';
$string['forassessment'] = '평가를 위해';
$string['format.listchildren'] = '이 작품에 대한 하위 항목 표시';
$string['format.listself'] = '아이템 목록표시 (다음과 같은 블록에 여러개의 작품을 놓을 수 있습니다)';
$string['format.renderfull'] = '모든 작품 표시';
$string['format.rendermetadata'] = '이 작품에 대해 메타데이터 표시';
$string['friend'] = '친구';
$string['friends'] = '친구들';
$string['friendslower'] = '친구들';
$string['grouplower'] = '모둠';
$string['groups'] = '모둠들';
$string['groupviews'] = '모둠 보여주기';
$string['in'] = ':';
$string['institutionviews'] = '기관 보여주기';
$string['invalidcolumn'] = '컬럼 %s 범위 초과';
$string['inviteusertojoingroup'] = '모둠에 가입하도록 사용자 초청';
$string['listviews'] = '보여주기 열거';
$string['loggedin'] = '로그인한 사용자';
$string['loggedinlower'] = '로그인한 사용자';
$string['makeprivate'] = '비공개로 변경';
$string['makepublic'] = '공개함';
$string['moveblockdown'] = '%s 블록을 아래로 이동';
$string['moveblockleft'] = '%s 블록을 왼쪽으로 이동';
$string['moveblockright'] = '%s 블록을 오른쪽으로 이동';
$string['moveblockup'] = '%s 블록을 위로 이동';
$string['movethisblockdown'] = '이 블록을 아래로 이동';
$string['movethisblockleft'] = '이 블록을 왼쪽으로 이동';
$string['movethisblockright'] = '이 블록을 오른쪽으로 이동';
$string['movethisblockup'] = '이 블록을 위로 이동';
$string['myviews'] = '내 보여주기';
$string['next'] = '다음';
$string['noaccesstoview'] = '이 보여주기를 접근할 수 있는 권한이 없습니다.';
$string['noartefactstochoosefrom'] = '죄송합니다. 선택할 작품이 없습니다.';
$string['noblocks'] = '죄송합니다. 이 범주에 블록이 없습니다.';
$string['nobodycanseethisview2'] = '당신만 이 보여주기를 볼 수 있습니다.';
$string['nochildren'] = '아무 작품도 없음';
$string['nocopyableviewsfound'] = '복사할 수 있는 보여주기가 없습니다.';
$string['noownersfound'] = '소유자가 없습니다.';
$string['nopublicfeedback'] = '공개 피드백이 없음';
$string['notifysiteadministrator'] = '사이트 관리자에게 통지';
$string['notitle'] = '제목 없음';
$string['noviewlayouts'] = '%s 컬럼 보여주기를 위한 보여주기 레이아웃이 없습니다.';
$string['noviews'] = '보여주기 없음';
$string['numberofcolumns'] = '컬럼의 수';
$string['overridingstartstopdate'] = '시작/종료일 덮어쓰기';
$string['overridingstartstopdatesdescription'] = '원하면 시작일과 종료일을 설정할 수 있습니다. 다른 사람들은 부여받은 접근 권한에 관계 없이 시작일 이전과 종료일 이후에는 보여주기를 볼 수 없습니다.';
$string['owner'] = '소유자';
$string['ownerformat'] = '이름 표시 형식';
$string['ownerformatdescription'] = '이 항목은 2번째 단계에서 선택한 템플릿에 대한 제작자 항목에 나타날 항목을 제어합니다.';
$string['owners'] = '소유자';
$string['placefeedback'] = '피드백 하기';
$string['placefeedbacknotallowed'] = '이 보여주기에 대해 피드백이 허용되지 않습니다.';
$string['print'] = '인쇄';
$string['profileicon'] = '자기소개 아이콘';
$string['profileviewtitle'] = '자기소개 보여주기';
$string['public'] = '공개';
$string['publiclower'] = '공개';
$string['reallyaddaccesstoemptyview'] = '보여주기에 블록이 없습니다. 다음 사용자들에게 이 보여주기에 대한 접근 권한을 주시겠습니까?';
$string['remove'] = '제거';
$string['removeblock'] = '%s 블록 제거';
$string['removecolumn'] = '이 열을 제거';
$string['removedfromwatchlist'] = '이 보여지기가 관심목록에서 제거되었습니다.';
$string['removefromwatchlist'] = '관심목록에서 보여지기 제거';
$string['removethisblock'] = '이 블록 제거';
$string['reportobjectionablematerial'] = '유해한 자료 보고';
$string['reportsent'] = '보고서가 보내어 졌습니다.';
$string['searchowners'] = '소유자 검색';
$string['searchviews'] = '보여주기 검색';
$string['searchviewsbyowner'] = '사용자별로 보여주기 검색';
$string['selectaviewtocopy'] = '복사하고자 하는 보여주기 선택';
$string['show'] = '보여주기';
$string['startdate'] = '접근 시작 일시';
$string['startdatemustbebeforestopdate'] = '시작일은 종료일 이전에 있어야 합니다.';
$string['stopdate'] = '접근 종료 일시';
$string['submitthisviewto'] = '이 보여주기를 다음에 제출:';
$string['submitviewconfirm'] = '평가를 위해\'%s\'를 \'%s\'에게 제출하면 튜터가 보여주기 평가가 끝나기 전까지는 보여주기를 편집할 수 없습니다. 보여주기를 제출하기를 원하십니까?';
$string['submitviewtogroup'] = '평가를 위해 \'%s\' 를 \'%s\'에게 제출';
$string['success.addblocktype'] = '블록을 성공적으로 추가하였습니다.';
$string['success.addcolumn'] = '컬럼을 성공적으로 추가하였습니다.';
$string['success.moveblockinstance'] = '블록을 성공적으로 이동하였습니다.';
$string['success.removeblockinstance'] = '블록을 성공적으로 삭제하였습니다.';
$string['success.removecolumn'] = '컬럼을 성공적으로 삭제하였습니다.';
$string['templatedescription'] = '당신의 보여주기를 볼 수 있는 사람들이 보여주기를 복사할 수 있도록 하려면 이 박스를 체크하세요.';
$string['thisfeedbackisprivate'] = '이 피드백은 비공개입니다.';
$string['thisfeedbackispublic'] = '이 피드백은 공개입니다.';
$string['thisviewmaybecopied'] = '복사가 허용되었습니다.';
$string['title'] = '보여주기 제목';
$string['token'] = '비밀 URL';
$string['tutors'] = '튜터';
$string['unrecogniseddateformat'] = '알수없는 날자 형식';
$string['updatewatchlistfailed'] = '관심목록 갱신이 실패하였습니다.';
$string['users'] = '사용자';
$string['usethistemplate'] = '이 템플릿을 선택';
$string['view'] = '보여주기';
$string['viewaccesseditedsuccessfully'] = '보여주기 접근권한이 성공적으로 저장되었습니다.';
$string['viewcolumnspagedescription'] = '처음에는 보여주기에서 컬럼수를 선택하고, 다음단계에서 컬럼의 폭을 변경할 수 있습니다.';
$string['viewcopywouldexceedquota'] = '이 파일을 복사하면 파일 할당량이 초과됩니다.';
$string['viewcreatedsuccessfully'] = '전시가 성공적으로 생성되었습니다';
$string['viewdeleted'] = '보여주기가 삭제되었습니다.';
$string['viewfilesdirdesc'] = '복사된 보여주기의 파일';
$string['viewfilesdirname'] = '보여주기파일';
$string['viewinformationsaved'] = '전시 정보가 성공적으로 저장되었습니다';
$string['viewlayoutchanged'] = '보여주기 레이아웃이 변경되었습니다.';
$string['viewlayoutpagedescription'] = '보여주기에서 컬럼이 어떻게 놓여질지를 선택하세요.';
$string['views'] = '보여주기';
$string['viewsavedsuccessfully'] = '보여주기가 성공적으로 저장되었습니다.';
$string['viewsby'] = '%s의 보여주기';
$string['viewscopiedfornewgroupsmustbecopyable'] = '보여주기를 다른 사용자가 복사할 수 있도록 하기 위해서는 복사를 허용해야 합니다.';
$string['viewscopiedfornewusersmustbecopyable'] = '보여주기를 다른 사용자가 복사할 수 있도록 하기 위해서는 복사를 허용해야 합니다.';
$string['viewsownedbygroup'] = '이 모둠이 소유한 보여주기';
$string['viewssharedtogroup'] = '이 모둠에 공유된 보여주기';
$string['viewssharedtogroupbyothers'] = '다른사람에 의해서 이 모둠에 공유된 보여주기';
$string['viewssubmittedtogroup'] = '이 모둠으로 제출된 보여주기';
$string['viewsubmitted'] = '보여주기가 제출되었습니다.';
$string['viewsubmittedtogroup'] = '이 보여주기가 <a href="%s">%s</a> 로 제출되었습니다.';
$string['watchlistupdated'] = '관심목록이 갱신되었습니다.';
$string['whocanseethisview'] = '이 보여주기를 볼 수 있는 사람';
$string['youhavenoviews'] = '보여주기가 없습니다.';
$string['youhaveoneview'] = '1개 보여주기가 있습니다.';
$string['youhaveviews'] = '%s 보여주기가 있습니다.';
?>
