<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Done'] = '완료';
$string['Export'] = '내보내기';
$string['Starting'] = '시작';
$string['allmydata'] = '내 모든 자료';
$string['chooseanexportformat'] = '내보내기 형식 선택';
$string['clicktopreview'] = '미리보기하려면 클릭';
$string['creatingzipfile'] = 'zip 파일 만들기';
$string['exportgeneratedsuccessfully'] = '내보내기가 성공적으로 만들어졌습니다. %s 다운로드하려면 여기를 클릭 %s';
$string['exportgeneratedsuccessfullyjs'] = '내보내기가 성공적으로 만들어졌습니다. %s 계속 %s';
$string['exportingartefactplugindata'] = '작품 플러그인 자료 내보내기';
$string['exportingartefacts'] = '작품 내보내기';
$string['exportingartefactsprogress'] = '작품 내보내기 %s/%s';
$string['exportingfooter'] = 'footer 내보내기';
$string['exportingviews'] = '보여주기 내보내기';
$string['exportingviewsprogress'] = '보여주기 내보내기: %s/%s';
$string['exportpagedescription'] = '이포트폴리오를 내보내기 할 수 있습니다. 이 도구로 이포트폴리오의 모든 정보와 보여주기를 내보내기 할 수 있습니다. 하지만 사이트 설정은 내보내기 하지 않습니다.';
$string['exportyourportfolio'] = '이포트폴리오 내보내기';
$string['generateexport'] = '내보내기 생성';
$string['justsomeviews'] = '내 보여주기중 몇개';
$string['noexportpluginsenabled'] = '관리자에 의해 내보내기 플러그인이 활성화되지 않아서 이 기능을 사용할 수 없습니다.';
$string['pleasewaitwhileyourexportisbeinggenerated'] = '내보내기가 생성되는 동안 잠시만 기다려 주세요.';
$string['setupcomplete'] = '설치 완료';
$string['unabletoexportportfoliousingoptions'] = '선택된 옵션으로 이포트폴리오 내보내기 할 수 없음';
$string['unabletogenerateexport'] = '내보내기 생성 할 수 없음';
$string['viewstoexport'] = '내보내기할 보여주기';
$string['whatdoyouwanttoexport'] = '무엇을 내보내기 하기를 원하십니까?';
$string['writingfiles'] = '파일 쓰는 중';
$string['youarehere'] = '현 위치';
$string['youmustselectatleastoneviewtoexport'] = '내보내기할 보여주기를 최소한 한개 이상 선택해야 합니다.';
$string['zipnotinstalled'] = '시스템이 zip 명령을 지원하지 않습니다. 이 기능을 활성화 하기 위해서는 zip을 설치하세요.';
?>
