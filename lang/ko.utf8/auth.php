<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addauthority'] = 'Authority 추가';
$string['application'] = '어플리케이션';
$string['authloginmsg'] = '마하라의 로그인 양식을 사용하여 로그인하고자 할때 표시할 메세지를 입력하세요.';
$string['authname'] = 'Authority 이름';
$string['cannotremove'] = '이 인증플러그인은 이 기관에서 사용하는 유일한 플러그인이므로 제거할 수 없습니다.';
$string['cannotremoveinuse'] = '이 인증플러그인을 사용하는 사용자가 있으므로 이 플러그인을 제거할 수 없습니다. 이 플러그인을 제거하려면 이들 사용자의 기록을 갱신해야 합니다.';
$string['cantretrievekey'] = 'An error occurred while retrieving the public key from the remote server.<br>Please ensure that the Application and WWW Root fields are correct, and that networking is enabled on the remote host.';
$string['changepasswordurl'] = '암호 변경 URL';
$string['editauthority'] = 'Authority 편집';
$string['errnoauthinstances'] = '%s에 호스트를 위해 설정한 인증 플러그인 인스턴스가 없는 것 같습니다.';
$string['errnoxmlrpcinstances'] = '%s에 호스트를 위해 설정된 XMLRPC 인증 플러그인 인스턴스가 없는 것 같습니다.';
$string['errnoxmlrpcuser'] = 'We were unable to authenticate you at this time. Possible reasons might be: * Your SSO session might have expired. Go back to the other application and click the link to sign into Mahara again. * You may not be allowed to SSO to Mahara. Please check with your administrator if you think you should be allowed to.';
$string['errnoxmlrpcwwwroot'] = '%s에 어떤 호스트에 대한 기록도 없습니다.';
$string['errorcertificateinvalidwwwroot'] = '이 인증서는 %s를 위한 것이지만, %s를 위해 사용하려고 하고 있습니다.';
$string['errorcouldnotgeneratenewsslkey'] = 'ssl키를 생성할 수 없습니다. openssl과 openssl을 위한 php모듈이 서버에 설치되었는지 확인하십시요.';
$string['errornotvalidsslcertificate'] = 'SSL 인증서에 문제가 있습니다.';
$string['host'] = '호스트이름 또는 주소';
$string['hostwwwrootinuse'] = 'WWW root가 다른 기관(%s)에서 이미 사용중입니다.';
$string['ipaddress'] = 'IP 주소';
$string['name'] = '사이트 이름';
$string['noauthpluginconfigoptions'] = '이 플러그인과 연관된 설정 옵션이 없습니다.';
$string['nodataforinstance'] = '인증 인스턴스에 대한 데이터를 찾을 수 없습니다.';
$string['parent'] = '상위 Authority';
$string['port'] = '포트번호';
$string['protocol'] = '프로토콜';
$string['requiredfields'] = '필요한 개인정보 항목';
$string['requiredfieldsset'] = '필요한 개인정보 항목 집합';
$string['saveinstitutiondetailsfirst'] = '인증 플러그인을 설정하기전에 기관의 세부사항을 저장하십시요.';
$string['shortname'] = '사이트에 대한 간단한 이름';
$string['ssodirection'] = 'SSO 방향';
$string['theyautocreateusers'] = '상대방이 사용자를 자동으로 생성합니다.';
$string['theyssoin'] = '상대방이 SSO로 접속합니다.';
$string['unabletosigninviasso'] = 'SSO로 접속할 수 없습니다.';
$string['updateuserinfoonlogin'] = '로그인할때 사용자 정보 갱신';
$string['updateuserinfoonlogindescription'] = '원격 서버에서 사용자 정보를 가져와서 사용자가 로그인할때마다 사용자 기록을 갱신';
$string['weautocreateusers'] = '우리가 사용자를 자동으로 생성합니다.';
$string['weimportcontent'] = '우리가 콘텐츠를 가져옵니다.';
$string['weimportcontentdescription'] = '(몇 어플리케이션만)';
$string['wessoout'] = '우리가';
$string['wwwroot'] = 'WWW root';
$string['xmlrpccouldnotlogyouin'] = '죄송합니다. 로그인해 드릴 수 없습니다:(';
$string['xmlrpccouldnotlogyouindetail'] = '죄송합니다. 마하라에 로그인 해 드릴 수 없습니다. 잠시후에 다시 시도해 보십시요. 문제가 지속되면 관리자에게 연락하십시요.';
$string['xmlrpcserverurl'] = 'XML-RPC 서버 URL';
?>
