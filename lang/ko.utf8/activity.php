<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addedcommunitytowatchlist'] = '커뮤니티가 관심목록에 추가되었습니다';
$string['addtowatchlist'] = '관심목록에 추가';
$string['alltypes'] = '모든 형식';
$string['andchildren'] = '아이들이 관찰되고 있습니다.';
$string['artefacts'] = '작품들';
$string['attime'] = '시각';
$string['communities'] = '커뮤니티들';
$string['date'] = '날자';
$string['deleteallnotifications'] = '모든 통지 삭제';
$string['deletednotifications'] = '%s 통지를 삭제';
$string['failedtodeletenotifications'] = '통지를 삭제하는데 실패하였습니다.';
$string['failedtomarkasread'] = '통지 읽음으로 표시 실패';
$string['groups'] = '모둠';
$string['institutioninvitemessage'] = '기관 설정페이지에서 이 기관의 구성원이 되었음을 확인할 수 있습니다.';
$string['institutioninvitesubject'] = '기관 %s에 가입하도록 초청받았습니다.';
$string['institutionrequestmessage'] = '기관 구성원 페이지에서 사용자들을 기관에 추가할 수 있습니다.';
$string['institutionrequestsubject'] = '%s가 %s의 구성원이 되기를 요청 하였습니다.';
$string['markasread'] = '읽은것으로 표시';
$string['markedasread'] = '통지 읽음으로 표시';
$string['missingparam'] = '활동 타입 %s에 필요한 파라메터가 비어있습니다.';
$string['monitored'] = '관찰됨';
$string['newcommunitymembersubj'] = '%s는 커뮤티티 멤버가 아닙니다!';
$string['newcontactus'] = '새 연락처';
$string['newcontactusfrom'] = '새 연락처';
$string['newfeedbackonartefact'] = '작품에 대한 새로운 소감';
$string['newfeedbackonview'] = '전시에 대한 새로운 소감';
$string['newgroupmembersubj'] = '%s는 모둠 구성원입니다.';
$string['newviewaccessmessage'] = '당신은 요청된 전시에 대한 접근 목록에 추가되었습니다';
$string['newviewaccesssubject'] = '새 전시 접근';
$string['newviewmessage'] = '요청된 관람 "%s"을 만들었고 당신에게 접근 권한을 부여하였습니다.';
$string['newviewsubject'] = '새 보기가 만들어졌습니다';
$string['newwatchlistmessage'] = '관심 목록상의 새로운 활동';
$string['newwatchlistmessageview'] = '%s 가  전시 %s를 변경하였습니다.';
$string['objectionablecontentartefact'] = '반대할 만한 콘텐츠';
$string['objectionablecontentview'] = '반대할 만한 콘텐츠';
$string['onartefact'] = '작품에서';
$string['oncommunity'] = '커뮤니티에서';
$string['ongroup'] = '다음 모둠에';
$string['onview'] = '전시에서';
$string['ownedby'] = '소유자';
$string['prefsdescr'] = '만일 이메일 옵션중에 어느하나를 선택하더라도 통지가 활동 로그에 입력될 것이지만 자동적으로 읽은 것으로 표시될 것입니다';
$string['read'] = '읽음';
$string['reallydeleteallnotifications'] = '모든 통지를 삭제하기를 원하십니까?';
$string['recurseall'] = '모두 반복';
$string['removedcommunityfromwatchlist'] = '이 커뮤니티는 관심목록에서 제외되었습니다';
$string['removedcommunitymembersubj'] = '%s는 더 이상 커뮤니티 멤버가 아닙니다';
$string['removedgroupmembersubj'] = '%s는 더 이상 모둠 구성원이 아닙니다.';
$string['removefromwatchlist'] = '관심목록에서 제외';
$string['selectall'] = '모두 선택';
$string['stopmonitoring'] = '관찰 중지';
$string['stopmonitoringfailed'] = '관찰 중지 실패';
$string['stopmonitoringsuccess'] = '관찰을 성공적으로 중지하였습니다';
$string['subject'] = '제목';
$string['type'] = '활동 형식';
$string['typeadminmessages'] = '관리 메세지';
$string['typecontactus'] = '연락처';
$string['typefeedback'] = '피드백';
$string['typegroupmessage'] = '모둠 메세지';
$string['typeinstitutionmessage'] = '기관 메세지';
$string['typemaharamessage'] = '시스템 메세지';
$string['typenewview'] = '새 전시';
$string['typeobjectionable'] = '반대할 만한 콘텐츠';
$string['typeusermessage'] = '다른 사용자로부터의 메세지';
$string['typeviewaccess'] = '새 전시 접근';
$string['typevirusrelease'] = '바이러스 플래그 배포';
$string['typevirusrepeat'] = '바이러스 업로드 반복';
$string['typewatchlist'] = '관심목록';
$string['unread'] = '읽지 않음';
$string['viewmodified'] = '전시를 변경하였습니다.';
$string['views'] = '전시들';
$string['viewsandartefacts'] = '전시와 작품';
$string['viewsubmittedmessage'] = '%s 전시 %s를 %s에게 제출함';
$string['viewsubmittedsubject'] = '전시가 %s에게 제출됨';
$string['watchlistmessageartefact'] = '관심 목록상의 활동 (작품)';
$string['watchlistmessagecommunity'] = '관심 목록상의 활동 (커뮤니티)';
$string['watchlistmessageview'] = '관심 목록상의 활동 (전시)';
?>
