<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['All'] = '모두';
$string['Artefact'] = '작품';
$string['Artefacts'] = '작품';
$string['Close'] = '닫기';
$string['Copyof'] = '%s 의 사본';
$string['Failed'] = '실패함';
$string['From'] = '부터';
$string['Help'] = '도움';
$string['Invitations'] = '초청';
$string['Memberships'] = '구성원자격';
$string['Permissions'] = '허가';
$string['Query'] = '쿼리';
$string['Requests'] = '요청';
$string['Results'] = '결과';
$string['Site'] = '사이트';
$string['Tag'] = '태그';
$string['To'] = '까지';
$string['View'] = 'View';
$string['about'] = '정보';
$string['accept'] = '승인';
$string['acceptinvitecommunity'] = '승인';
$string['accessforbiddentoadminsection'] = '관리 영역을 접근하는것이 금지되었습니다';
$string['accessstartdate'] = 'Access start date';
$string['accessstopdate'] = 'Access end date';
$string['accesstotallydenied_institutionsuspended'] = '당신의 기관%s 이 사용중지 중입니다. 사용중지 해지 될때 까지 %s에 로그인 할 수 없습니다. 지원을 받으려면 기관에게 문의하십시요.';
$string['account'] = '내 계정';
$string['accountcreated'] = '%s: 새 계정';
$string['accountcreatedchangepasswordhtml'] = '<p>안녕하세요? %s,</p> <p>새로운 계정이 <a href="%s">%s</a>에 만들어졌습니다. 당신의 세부 정보는 다음과 같습니다</p> <ul> <li><strong>사용자이름:</strong> %s</li> <li><strong>암호:</strong> %s</li> </ul> <p>처음 로그인 하면 암호를 변경하도록 요청받을 것입니다.</p> <p>시작하기 위해 <a href="%s">%s</a> 를 방문하십시요</p> 
<p>좋은 하루 되시길, %s 사이트관리자</p>';
$string['accountcreatedchangepasswordtext'] = '안녕하세요? %s,새로운 계정이 %s에 만들어졌습니다. 당신의 세부 정보는 다음과 같습니다. 사용자이름:%s 암호:%s 처음 로그인 하면 암호를 변경하도록 요청받을 것입니다.시작하기 위해 %s를 방문하십시요. 좋은 하루 되시길, %s 사이트관리자';
$string['accountcreatedhtml'] = '<p>안녕하세요? %s,</p> <p>새로운 계정이 <a href="%s">%s</a>에 만들어졌습니다. 당신의 세부 정보는 다음과 같습니다</p> <ul> <li><strong>사용자이름:</strong> %s</li> <li><strong>암호:</strong> %s</li> </ul> <p>시작하기 위해 <a href="%s">%s</a> 를 방문하십시요</p> 
<p>좋은 하루 되시길, %s 사이트관리자</p>';
$string['accountcreatedtext'] = '안녕하세요? %s,새로운 계정이 %s에 만들어졌습니다. 당신의 세부 정보는 다음과 같습니다사용자이름:%s 암호:%s. 시작하기 위해 %s 를 방문하십시요. 좋은 하루 되시길, %s 사이트관리자';
$string['accountdeleted'] = '죄송합니다. 당신의 계정이 삭제되었습니다';
$string['accountexpired'] = '죄송합니다. 당신의 계정이 만료되었습니다';
$string['accountexpirywarning'] = '계정 만료 경고';
$string['accountexpirywarninghtml'] = '<p>안녕하세요 %s,</p>
    
<p>%s 에 있는 당신의 계정이 %s 이전에 만료됩니다.</p>

<p>내보내기 도구를 사용하여 당신의 포트폴리오의 내용을 저장할 것을 권장합니다. 이 기능을 사용하는 것에 대한 지시사항은 사용자 안내서에서 찾을 수 있습니다.</p>

<p>당신의 계정을 연장하기를 원하거나 질문이 있는 경우 연락 주십시요. <a href="%s">연락처</a>.</p>

<p>좋은 하루 되시길 빕니다. %s 사이트 관리자</p>';
$string['accountexpirywarningtext'] = '안녕하세요 %s,

%s 에 있는 당신의 계정이 %s 이전에 만료됩니다.

내보내기 도구를 사용하여 당신의 포트폴리오의 내용을 저장할 것을 권장합니다. 이 기능을 사용하는 것에 대한 지시사항은 사용자 안내서에서 찾을 수 있습니다.

당신의 계정을 연장하기를 원하거나 질문이 있는 경우 연락 주십시요.

연락처:%s

좋은 하루 되시길 빕니다. %s 사이트 관리자';
$string['accountinactive'] = '죄송합니다. 당신의 계정은 현재 비활성화 되어있습니다';
$string['accountinactivewarning'] = '계정 비활성화 경고';
$string['accountinactivewarninghtml'] = '<p>안녕하세요 %s,</p>

<p>%s 에 있는 당신의 계정이 %s 이전에 비활성화 됩니다.</p>

<p>일단 비활성화되면 관리자가 당신의 계정을 활성화 하기전에는 로그인 할 수 없게 됩니다.</p>

<p>로그인하면 당신의 계정이 비활성화 되는 것을 방지할 수 있습니다.</p>

<p>좋은 하루 되시길 빕니다. %s 사이트 관리자</p>';
$string['accountinactivewarningtext'] = '안녕하세요 %s,

%s 에 있는 당신의 계정이 %s 이전에 비활성화 됩니다.

일단 비활성화되면 관리자가 당신의 계정을 활성화 하기전에는 로그인 할 수 없게 됩니다.

로그인하면 당신의 계정이 비활성화 되는 것을 방지할 수 있습니다.

좋은 하루 되시길 빕니다. %s 사이트 관리자';
$string['accountprefs'] = '선택 설정';
$string['accountsuspended'] = '당신의 계정이 %s 현재 사용중지 되었습니다. 사용중지 사유는<blockquote>%s</blockquote> 입니다.';
$string['activity'] = '최근 활동';
$string['activityprefs'] = '활동 선호사항';
$string['add'] = '추가';
$string['addcommunity'] = '새 커뮤니티 추가';
$string['addedtocommunitymessage'] = '%s has added you to a community, \'%s\'.  Click on the link below to see the community';
$string['addedtocommunitysubject'] = 'You were added to a community';
$string['addedtofriendslistmessage'] = '%s added you as a friend! This means that %s is also on your friend list now too.  Click on the link below to see their profile page';
$string['addedtofriendslistsubject'] = '새 친구';
$string['addedtowatchlist'] = 'This %s has been added to your watchlist';
$string['addemail'] = '이메일 주소 추가';
$string['addfeedbackfailed'] = 'Add feedback failed';
$string['addtofriendslist'] = 'Add to friends';
$string['addtowatchlist'] = 'Add %s to watchlist';
$string['addtowatchlistwithchildren'] = 'Add entire %s contents to watchlist';
$string['adduserfailed'] = 'Failed to add the user';
$string['addusertocommunity'] = '커뮤니티에 이 사용자 추가';
$string['adminphpuploaderror'] = '파일 올리기 오류가 서버 설정에 의해 발생한 것 같습니다.';
$string['advancedsearch'] = '고급 검색';
$string['allowpublicaccess'] = '공개(로그인 하지 않은) 접근 허용';
$string['alltags'] = '모든 태그';
$string['allusers'] = '모든 사용자';
$string['allviews'] = 'All views';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,ㄱ,ㄴ,ㄷ,ㄹ,ㅁ,ㅂ,ㅅ,ㅇ,ㅈ,ㅊ,ㅋ,ㅌ,ㅍ,ㅎ';
$string['alreadyinwatchlist'] = 'This %s is already in your watchlist';
$string['approve'] = '승인';
$string['artefact'] = '작품';
$string['artefactnotfound'] = '아이디가 %s 인 작품을 찾을 수 없습니다';
$string['artefactnotpublishable'] = '작품 %s는 보여주기 %s에 출판할 수 없습니다.';
$string['artefactnotrendered'] = '작품이 렌더링되지 않았습니다';
$string['artefacts'] = '작품';
$string['at'] = '장소';
$string['attachedfileaddedtofolder'] = 'The attached file %s has been added to your \'%s\' folder.';
$string['attachfile'] = '파일 첨부';
$string['back'] = '뒤로';
$string['backto'] = '%s로 돌아가기';
$string['belongingto'] = '소유자';
$string['bytes'] = '바이트';
$string['cancel'] = '취소';
$string['cancelrequest'] = '요청 취소';
$string['cannotremovedefaultemail'] = '주 이메일 주소를 제거할 수 없습니다.';
$string['cantchangepassword'] = 'Sorry, you are unable to change your password through this interface - please use your institution\'s interface instead';
$string['cantdeletegroupdontown'] = 'You can\'t delete this group, you don\'t own it';
$string['canteditdontown'] = '당신이 모둠을 소유하고 있지 않으므로 편집할 수 없습니다.';
$string['cantremovedefaultemail'] = '당신의 주 이메일 주소를 제거할 수 없습니다';
$string['captchadescription'] = '오른쪽 그림 안에 보이는 문자들을 입력하세요. 대소문자 구별안함';
$string['captchaimage'] = 'CAPTCHA 이미지';
$string['captchaincorrect'] = '이미지에 보이는 대로 문자들을 입력하세요';
$string['captchatitle'] = 'CAPTCHA 이미지';
$string['change'] = '변경';
$string['changepassword'] = '암호 변경';
$string['changepasswordinfo'] = '계속하기전에 암호를 변경해야 합니다.';
$string['chooseusernamepassword'] = '사용자 이름과 암호를 선택하세요.';
$string['chooseusernamepasswordinfo'] = '%s 로그인하기 위해 사용자 이름과 암호가 필요합니다. 지금 선택하세요.';
$string['clambroken'] = 'Your administrator has enabled virus checking for file uploads but has misconfigured something.  Your file upload was NOT successful. Your administrator has been emailed to notify them so they can fix it.  Maybe try uploading this file later.';
$string['clamdeletedfile'] = '파일이 삭제되었습니다.';
$string['clamdeletedfilefailed'] = '파일이 삭제될 수 없습니다.';
$string['clamemailsubject'] = '%s :: Clam AV 통지';
$string['clamfailed'] = 'Clam AV has failed to run.  The return error message was %s. Here is the output from Clam:';
$string['clamlost'] = 'Clam AV is configured to run on file upload, but the path supplied to Clam AV, %s, is invalid.';
$string['clammovedfile'] = '파일이 검역 폴더로 이동되었습니다.';
$string['clamunknownerror'] = '알려지지 않은 clam 오류가 있습니다.';
$string['collapse'] = '축약';
$string['communities'] = '커뮤니티';
$string['communityalreadyexists'] = 'A Community by this name already exists';
$string['communityconfirmdelete'] = 'Are you sure you wish to delete this community?';
$string['communityconfirmdeletehasviews'] = 'Are you sure you wish to delete this community? Some of your views use this community for access control, removing this community would mean that the members of that community would not have access to the views.';
$string['communitydescription'] = 'Community Description';
$string['communityhaveinvite'] = 'You have been invited to join this community';
$string['communityinviteaccepted'] = 'Invite accepted successfully! You are now a community member';
$string['communityinvitedeclined'] = 'Invite declined successfully!';
$string['communityjointypecontrolled'] = 'Membership to this community  is controlled.  You cannot join this community';
$string['communityjointypeinvite'] = 'Membership to this community is invite only';
$string['communityjointypeopen'] = 'Membership to this community is open';
$string['communityjointyperequest'] = 'Membership to this community is by request only';
$string['communitymemberrequests'] = 'Pending membership requests';
$string['communitymembershipchangedmessageaddedmember'] = 'You have been added as a member in this community';
$string['communitymembershipchangedmessageaddedtutor'] = 'You have been added as a tutor in this community';
$string['communitymembershipchangemessagedeclinerequest'] = 'Your request to join this community has been declined';
$string['communitymembershipchangemessagemember'] = 'You have been demoted from a tutor in this community';
$string['communitymembershipchangemessageremove'] = 'You have been removed from this community';
$string['communitymembershipchangemessagetutor'] = 'You have been promoted to a tutor in this community';
$string['communitymembershipchangesubject'] = 'Community membership: %s';
$string['communityname'] = 'Community Name';
$string['communitynotinvited'] = 'You have not been invited to join this community';
$string['communityrequestmessage'] = '%s has sent a membership request to join the community %s';
$string['communityrequestmessagereason'] = '%s has sent a membership request to join the community %s with the reason %s.';
$string['communityrequestsent'] = 'Community membership request sent';
$string['communityrequestsubject'] = 'New community membership request';
$string['communitysaved'] = 'Community Saved Successfully';
$string['complaint'] = '불평';
$string['complete'] = '완료';
$string['config'] = '설정';
$string['confirmdeletegroup'] = 'Are you sure you want to delete this group?';
$string['confirmdeletegrouphasviews'] = 'Are you sure you want to delete this group? Some of your views use this group for access control, removing this group would mean that the members of that group would not have access to the views.';
$string['confirmdeletetag'] = '이포트폴리오에 있는 모든 것에서 이 태그를 삭제하기를 원하십니까?';
$string['confirminvitation'] = '초청 확인';
$string['confirmpassword'] = '암호 확인';
$string['confirmremovefriend'] = 'Are you sure you want to remove this user from your friends list?';
$string['contactus'] = '연락처';
$string['cookiesnotenabled'] = '브라우저가 쿠키 활성화 되어 있지 않거나 차단하고 있습니다. 로그인 하려면 쿠키가 활성화 되어야 합니다';
$string['couldnotgethelp'] = '도움말 페이지를 가져오는데 오류가 발생하였습니다.';
$string['couldnotjoincommunity'] = 'You cannot join this community';
$string['couldnotleavecommunity'] = 'You cannot leave this community';
$string['couldnotrequestcommunity'] = 'Could not send community membership request';
$string['country.ad'] = '안도라';
$string['country.ae'] = '연합에미리트';
$string['country.af'] = '아프가니스탄';
$string['country.ag'] = 'Antigua and Barbuda';
$string['country.ai'] = 'Anguilla';
$string['country.al'] = '알바니아';
$string['country.am'] = '아르메니아';
$string['country.an'] = 'Netherlands Antilles';
$string['country.ao'] = '앙골라';
$string['country.aq'] = '남극';
$string['country.ar'] = '아르헨티나';
$string['country.as'] = 'American Samoa';
$string['country.at'] = '오스트리아';
$string['country.au'] = '오스트랄리아';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Åland Islands';
$string['country.az'] = '아제르바이젠';
$string['country.ba'] = 'Bosnia and Herzegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = '방글라데시';
$string['country.be'] = '벨기에';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = '불가리아';
$string['country.bh'] = '바레인';
$string['country.bi'] = '브룬디';
$string['country.bj'] = 'Benin';
$string['country.bm'] = '버뮤다';
$string['country.bn'] = '브루나이';
$string['country.bo'] = '볼리비아';
$string['country.br'] = '브라질';
$string['country.bs'] = '바하마';
$string['country.bt'] = '부탄';
$string['country.bv'] = 'Bouvet Island';
$string['country.bw'] = '보츠와나';
$string['country.by'] = '벨라루스';
$string['country.bz'] = 'Belize';
$string['country.ca'] = '카나다';
$string['country.cc'] = 'Cocos (Keeling) Islands';
$string['country.cd'] = '콘고';
$string['country.cf'] = '중앙아프리카공화국';
$string['country.cg'] = '콩고';
$string['country.ch'] = '스위스';
$string['country.ci'] = 'Cote D\'ivoire';
$string['country.ck'] = '쿡아일랜드';
$string['country.cl'] = '칠레';
$string['country.cm'] = '카메룬';
$string['country.cn'] = '중국';
$string['country.co'] = '콜롬비아';
$string['country.cr'] = '코스타리카';
$string['country.cs'] = 'Serbia and Montenegro';
$string['country.cu'] = '큐바';
$string['country.cv'] = 'Cape Verde';
$string['country.cx'] = '크리스마스 아일랜드';
$string['country.cy'] = '시프러스';
$string['country.cz'] = '체코';
$string['country.de'] = '독일';
$string['country.dj'] = 'Djibouti';
$string['country.dk'] = '덴마크';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominican Republic';
$string['country.dz'] = '알제리';
$string['country.ec'] = '에쿠아도르';
$string['country.ee'] = '에스토니아';
$string['country.eg'] = '에집트';
$string['country.eh'] = '서부 사하라';
$string['country.er'] = 'Eritrea';
$string['country.es'] = '스페인';
$string['country.et'] = '에티오피아';
$string['country.fi'] = '핀란드';
$string['country.fj'] = '피지';
$string['country.fk'] = 'Falkland Islands (Malvinas)';
$string['country.fm'] = 'Micronesia, Federated States of';
$string['country.fo'] = 'Faroe Islands';
$string['country.fr'] = '프랑스';
$string['country.ga'] = '가봉';
$string['country.gb'] = '영국';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Georgia';
$string['country.gf'] = 'French Guiana';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = '가나';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = '그랜랜드';
$string['country.gm'] = 'Gambia';
$string['country.gn'] = 'Guinea';
$string['country.gp'] = 'Guadeloupe';
$string['country.gq'] = 'Equatorial Guinea';
$string['country.gr'] = '그리스';
$string['country.gs'] = 'South Georgia and The South Sandwich Islands';
$string['country.gt'] = '과테말라';
$string['country.gu'] = '괌';
$string['country.gw'] = 'Guinea-bissau';
$string['country.gy'] = 'Guyana';
$string['country.hk'] = '홍콩';
$string['country.hm'] = 'Heard Island and Mcdonald Islands';
$string['country.hn'] = '온두라스';
$string['country.hr'] = '크로아티아';
$string['country.ht'] = '하이티';
$string['country.hu'] = '헝가리';
$string['country.id'] = '인도네시아';
$string['country.ie'] = '아일랜드';
$string['country.il'] = '이스라엘';
$string['country.im'] = 'Isle of Man';
$string['country.in'] = '인디아';
$string['country.io'] = 'British Indian Ocean Territory';
$string['country.iq'] = '이라크';
$string['country.ir'] = '이란';
$string['country.is'] = '아이슬랜드';
$string['country.it'] = '이탈리아';
$string['country.je'] = 'Jersey';
$string['country.jm'] = '자마이카';
$string['country.jo'] = '요르단';
$string['country.jp'] = '일본';
$string['country.ke'] = '케냐';
$string['country.kg'] = 'Kyrgyzstan';
$string['country.kh'] = '캄보디아';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Comoros';
$string['country.kn'] = 'Saint Kitts and Nevis';
$string['country.kp'] = '북한';
$string['country.kr'] = '대한민국';
$string['country.kw'] = '쿠웨이트';
$string['country.ky'] = 'Cayman Islands';
$string['country.kz'] = '카자흐스탄';
$string['country.la'] = 'Lao People\'s Democratic Republic';
$string['country.lb'] = '레바논';
$string['country.lc'] = 'Saint Lucia';
$string['country.li'] = 'Liechtenstein';
$string['country.lk'] = '스리랑카';
$string['country.lr'] = '리베리아';
$string['country.ls'] = 'Lesotho';
$string['country.lt'] = '리투아니아';
$string['country.lu'] = '룩셈부르크';
$string['country.lv'] = '라트비아';
$string['country.ly'] = 'Libyan Arab Jamahiriya';
$string['country.ma'] = '모로코';
$string['country.mc'] = '모나코';
$string['country.md'] = '몰도바';
$string['country.mg'] = '마다카스카르';
$string['country.mh'] = '마샬군도';
$string['country.mk'] = '마케도니아';
$string['country.ml'] = '몰리';
$string['country.mm'] = '미얀마';
$string['country.mn'] = '몽고';
$string['country.mo'] = '마카오';
$string['country.mp'] = 'Northern Mariana Islands';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = '모리타니아';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = '몰타';
$string['country.mu'] = 'Mauritius';
$string['country.mv'] = '몰디브';
$string['country.mw'] = '말라위';
$string['country.mx'] = '멕시코';
$string['country.my'] = '말레이지아';
$string['country.mz'] = '모잠비크';
$string['country.na'] = 'Namibia';
$string['country.nc'] = '뉴칼레도니아';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Norfolk Island';
$string['country.ng'] = '나이제리아';
$string['country.ni'] = '니카라구아';
$string['country.nl'] = '네덜란드';
$string['country.no'] = '노르웨이';
$string['country.np'] = '네팔';
$string['country.nr'] = '나우루';
$string['country.nu'] = 'Niue';
$string['country.nz'] = '뉴질랜드';
$string['country.om'] = '오만';
$string['country.pa'] = '파나마';
$string['country.pe'] = '페루';
$string['country.pf'] = '불란서제도';
$string['country.pg'] = '파푸아 뉴기니아';
$string['country.ph'] = '필리핀';
$string['country.pk'] = '파키스탄';
$string['country.pl'] = '폴랜드';
$string['country.pm'] = 'Saint Pierre and Miquelon';
$string['country.pn'] = 'Pitcairn';
$string['country.pr'] = '푸에토리코';
$string['country.ps'] = 'Palestinian Territory, Occupied';
$string['country.pt'] = '포르트갈';
$string['country.pw'] = '팔라우';
$string['country.py'] = '파라구아이';
$string['country.qa'] = '카르타르';
$string['country.re'] = 'Reunion';
$string['country.ro'] = '루마니아';
$string['country.ru'] = '러시아 연방';
$string['country.rw'] = '르완다';
$string['country.sa'] = '사우디 아라비아';
$string['country.sb'] = '솔로몬제도';
$string['country.sc'] = 'Seychelles';
$string['country.sd'] = '수단';
$string['country.se'] = '스웨덴';
$string['country.sg'] = '싱가포르';
$string['country.sh'] = '세인트 헬레나';
$string['country.si'] = '슬로베니아';
$string['country.sj'] = 'Svalbard and Jan Mayen';
$string['country.sk'] = '슬로바키아';
$string['country.sl'] = 'Sierra Leone';
$string['country.sm'] = '산마리노';
$string['country.sn'] = '세네갈';
$string['country.so'] = '소말리아';
$string['country.sr'] = '수리남';
$string['country.st'] = 'Sao Tome and Principe';
$string['country.sv'] = '엘 살바도르';
$string['country.sy'] = '시리아';
$string['country.sz'] = 'Swaziland';
$string['country.tc'] = 'Turks and Caicos Islands';
$string['country.td'] = '챠드';
$string['country.tf'] = 'French Southern Territories';
$string['country.tg'] = '토고';
$string['country.th'] = '타이';
$string['country.tj'] = '타지키스탄';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Timor-leste';
$string['country.tm'] = '투르크메니스탄';
$string['country.tn'] = '튜니지아';
$string['country.to'] = '통가';
$string['country.tr'] = '터키';
$string['country.tt'] = 'Trinidad and Tobago';
$string['country.tv'] = '투발루';
$string['country.tw'] = '타이완';
$string['country.tz'] = '탄자니아';
$string['country.ua'] = '우크라이나';
$string['country.ug'] = '우간다';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.us'] = '미국';
$string['country.uy'] = '우루구아이';
$string['country.uz'] = '우즈베키스탄';
$string['country.va'] = 'Holy See (Vatican City State)';
$string['country.vc'] = 'Saint Vincent and The Grenadines';
$string['country.ve'] = '베네주엘라';
$string['country.vg'] = '버진 아일랜드(영)';
$string['country.vi'] = '버진 아일랜드(미)';
$string['country.vn'] = '베트남';
$string['country.vu'] = '바누아투';
$string['country.wf'] = 'Wallis and Futuna';
$string['country.ws'] = '사모아';
$string['country.ye'] = '예멘';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = '남아프리카';
$string['country.zm'] = '잠비아';
$string['country.zw'] = '짐바브웨';
$string['createcommunity'] = '커뮤니티 생성';
$string['created'] = '만들어짐';
$string['creategroup'] = '새 모둠 추가';
$string['createnewview'] = 'Create New View';
$string['currentfriends'] = '친구들';
$string['date'] = '날자';
$string['day'] = '일';
$string['days'] = '일';
$string['debugemail'] = '알림: 이 이메일은 %s <%s>에게 보내져야 하나, "sendallemailto" 설정에 의해 당신에게 보내어졌습니다.';
$string['decline'] = '거절';
$string['declineinvitecommunity'] = '거절';
$string['declinerequest'] = '요청 거절';
$string['default'] = '기본';
$string['delete'] = '삭제';
$string['deletecommunitysuccessful'] = 'Community Deleted Successfully';
$string['deleteduser'] = '삭제된 사용자';
$string['deletegroupsuccessful'] = 'Group successfully deleted';
$string['deletetag'] = '삭제 <a href="%s">%s</a>';
$string['deletetagdescription'] = '포트폴리오의 모든 항목에서 이 태그 제거';
$string['deleteviewfailed'] = 'Delete view failed';
$string['deleteviewquestion'] = 'Do you really want to delete this view?';
$string['description'] = '설명';
$string['disable'] = '비활성화';
$string['displayname'] = '이름 표시';
$string['done'] = '완료';
$string['edit'] = '편집';
$string['editaccess'] = '접근 편집';
$string['editcommunity'] = '커뮤니티 편집';
$string['editgroup'] = '모둠 편집';
$string['editing'] = '편집중';
$string['editmyprofilepage'] = '개인정보 페이지 편집';
$string['edittag'] = '편집 <a href="%s">%s</a>';
$string['edittagdescription'] = '포트폴리오에서 "%s" 로 태그된 모든 항목이 새로고침됩니다.';
$string['edittags'] = '태그 편집';
$string['editthistag'] = '이 태그 편집';
$string['editview'] = 'Edit View';
$string['editviewinformation'] = 'Edit View Information';
$string['email'] = '이메일';
$string['emailaddress'] = '이메일 주소';
$string['emailaddressdescription'] = '이메일 주소 설명';
$string['emailaddressorusername'] = '이메일 주소나 사용자 이름';
$string['emailname'] = '마하라 시스템';
$string['emailnotsent'] = '연락처 이메일 보내기 실패. 오류메세지: "%s"';
$string['emailtoolong'] = '이메일 주소는 255자 보다 길어서는 안됩니다.';
$string['enable'] = '활성화';
$string['errorprocessingform'] = '이 양식을 보내는데 오류가 발생하였습니다. 표시된 필드를 확인하고 다시 시도하십시요.';
$string['expand'] = '확대';
$string['feedback'] = '댓글';
$string['feedbackattachdirdesc'] = 'Files attached to view assessments';
$string['feedbackattachdirname'] = 'assessmentfiles';
$string['feedbackattachmessage'] = 'The attached file has been added to your %s folder';
$string['feedbackmadeprivate'] = 'Feedback changed to private';
$string['feedbackonthisartefactwillbeprivate'] = 'Feedback on this artefact will only be visible to the owner.';
$string['feedbackonviewbytutorofcommunity'] = 'Feedback on %s by %s of %s';
$string['feedbacksubmitted'] = 'Feedback submitted';
$string['filenotimage'] = '올린 파일은 유효한 이미지가 아닙니다. PNG, JPEG 또는 GIF 파일이어야 합니다.';
$string['filetypenotallowed'] = 'You are not allowed to upload files of this type. Please contact your administrator for more information.';
$string['fileunknowntype'] = '올린 파일 형식을 알 수 없습니다. 파일이 손상되었거나 설정문제인것 같습니다. 관리자에게 문의하십시요.';
$string['filter'] = '필터';
$string['filterresultsby'] = '결과를 거르기';
$string['findfriends'] = '친구 찾기';
$string['findgroups'] = '모둠 찾기';
$string['first'] = '첫번째';
$string['firstname'] = '이름';
$string['firstnamedescription'] = '이름 설명';
$string['firstpage'] = '첫 페이지';
$string['forgotpassemailmessagehtml'] = '<p>Dear %s,</p>

<p>A request to reset your password has been received for your %s account.</p>

<p>Please follow the link below to continue the reset process.</p>

<p><a href="http://folio.pcu.ac.kr/forgotpass.php?key=%s">http://folio.pcu.ac.kr/forgotpass.php?key=%s</a></p>

<p>If you did not request a password reset, please ignore this email.</p>

<p>If you have any questsions regarding the above, please feel free to <a href="http://folio.pcu.ac.kr/contact.php">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>

<p><a href="http://folio.pcu.ac.kr/forgotpass.php?key=%s">http://folio.pcu.ac.kr/forgotpass.php?key=%s</a></p>';
$string['forgotpassemailmessagetext'] = 'Dear %s,

A request to reset your password has been received for your %s account.

Please follow the link below to continue the reset process.

http://folio.pcu.ac.kr/forgotpass.php?key=%s

If you did not request a password reset, please ignore this email.

If you have any questsions regarding the above, please feel free to contact
us.

http://folio.pcu.ac.kr/contact.php

Regards, %s Site Administrator

http://folio.pcu.ac.kr/forgotpass.php?key=%s';
$string['forgotpassemailsendunsuccessful'] = '죄송합니다. 이메일이 성공적으로 전송되지 않은 것 같습니다. 이것은 우리책임이며 추후 다시 시도해 보십시요.';
$string['forgotpassemailsubject'] = 'Change password request for %s';
$string['forgotpassnosuchemailaddress'] = 'The email address you entered doesn\'t match any users for this site';
$string['forgotpassnosuchemailaddressorusername'] = '입력한 이메일 주소나 사용자 이름이 사이트의 사용자와 일치하는 것이 없습니다.';
$string['forgotpassword'] = 'Forgotten your password?';
$string['forgotpasswordenternew'] = '계속하기 위해 새로운 암호를 입력하세요.';
$string['forgotpasswordtext'] = 'If you have forgotten your password, enter below the primary email address you have listed in your Profile and we will send you a key you can use to give yourself a new password';
$string['forgotusernamepassword'] = '사용자이름 혹은 암호를 앚으셨나요?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>안녕하세요? %s,</p> <p> %s에 있는 계정에 대해 사용자이름/암호 요청이 왔습니다.</p> <p>사용자 이름은 <strong>%s</strong>입니다.</p> <p>암호를 재설정하고자 하면 다음 링크를 따라가세요:</p> <p><a href="%s">%s</a></p> <p>암호 재설정을 요청하지 않았으면 이 이메일을 무시하세요</p> <p>이에 대해 질문이 있으면  <a href="%s">연락</a> 주세요.</p> <p>좋은 하루 되시길, %s 사이트 관리자</p>';
$string['forgotusernamepasswordemailmessagetext'] = '안녕하세요? %s, %s에 있는 계정에 대해 사용자이름/암호 요청이 왔습니다.사용자 이름은 %s입니다.암호를 재설정하고자 하면 다음 링크를 따라가세요:%s 암호 재설정을 요청하지 않았으면 이 이메일을 무시하세요. 이에 대해 질문이 있으면 연락: %s 주세요.좋은 하루 되시길, %s 사이트 관리자</p>';
$string['forgotusernamepasswordemailsubject'] = '%s 에 대한 사용자이름/암호 세부사항';
$string['forgotusernamepasswordtext'] = '<p>사용자 이름 혹은 암호를 잊은 경우 개인정보에 입력한 이메일 주소를 입력하세요. 새로운 암호를 설정할 수 있는 정보를 보낼 것입니다.</p> <p>만일 사용자 이름을 알지만 암호를 잊은 경우 사용자 이름을 입력해도 됩니다.</p>';
$string['formatpostbbcode'] = '게시물을 BB코드를 사용하여 포맷할 수 있습니다. %s 자세한 사항 알아보기 %s';
$string['friend'] = '친구';
$string['friendformacceptsuccess'] = 'Accepted friend request';
$string['friendformaddsuccess'] = 'Added %s to your friends list';
$string['friendformrejectsuccess'] = 'Rejected friend request';
$string['friendformremovesuccess'] = 'Removed %s from your friends list';
$string['friendformrequestsuccess'] = 'Sent a friendship request to %s';
$string['friendlistfailure'] = 'Failed to modify your friends list';
$string['friendrequestacceptedmessage'] = '%s has accepted your friend request and they have been added to your friends list';
$string['friendrequestacceptedsubject'] = 'Friend request accepted';
$string['friendrequestrejectedmessage'] = '%s has rejected your friend request.';
$string['friendrequestrejectedmessagereason'] = '%s has rejected your friend request.  Their reason was:';
$string['friendrequestrejectedsubject'] = 'Friend request rejected';
$string['friends'] = '친구들';
$string['friendshipalreadyrequested'] = 'You have requested to be added to %s\'s friends list';
$string['friendshipalreadyrequestedowner'] = '%s has requested to be added to your friends list';
$string['fullname'] = '전체 이름';
$string['go'] = '진행';
$string['groupalreadyexists'] = '같은 이름의 모둠이 이미 존재합니다';
$string['groupcreated'] = '모둠이 만들어됨';
$string['groupdescription'] = '모둠 설명';
$string['groupmembers'] = '모둠 구성원';
$string['groupname'] = '모둠 이름';
$string['groups'] = '모둠';
$string['groupsaved'] = '모둠이 저장됨';
$string['height'] = '높이';
$string['heightshort'] = 'h';
$string['home'] = '홈';
$string['image'] = '이미지';
$string['importedfrom'] = '%s 에서 가져옴';
$string['incomingfolderdesc'] = '다른 네트워크된 호스트에서 가져온 파일들';
$string['installedplugins'] = '설치된 플러그인';
$string['institution'] = '기관';
$string['institutionexpirywarning'] = '기관 만료 경고';
$string['institutionexpirywarninghtml_institution'] = '<p>안녕하세요? %s,</p> <p>%s의 구성원자격(%s에서) %s에 만료됩니다.</p> <p>기관의 구성원 자격을 연장하거나 이에 대해 질문이 있으면 <a href="%s">연락처</a>로 연락주십시요.</p> <p>좋은 하루 되시길, %s 사이트관리자</p>';
$string['institutionexpirywarninghtml_site'] = '<p>안녕하세요? %s,</p> <p>기관 \'%s\'이 %s에 만료됩니다..</p> <p>%s의 구성원 자격을 연장하기 위해 연락해 보십시요</p> <p>좋은 하루 되시길, %s 사이트관리자</p>';
$string['institutionexpirywarningtext_institution'] = '안녕하세요? %s,%s의 구성원자격(%s에서) %s에 만료됩니다. 기관의 구성원 자격을 연장하거나 이에 대해 질문이 있으면 %s 로 연락주십시요. 좋은 하루 되시길, %s 사이트관리자';
$string['institutionexpirywarningtext_site'] = '안녕하세요? %s,기관 \'%s\'이 %s에 만료됩니다. %s의 구성원 자격을 연장하기 위해 연락해 보십시요. 좋은 하루 되시길, %s 사이트관리자';
$string['institutionfull'] = '선택한 기관은 더 이상 등록을 받지 않습니다.';
$string['institutionmemberconfirmmessage'] = '%s의 구성원으로 추가되었습니다.';
$string['institutionmemberconfirmsubject'] = '기관 구성원 자격 확인';
$string['institutionmemberrejectmessage'] = '%s 구성원 자격이 거절되었습니다.';
$string['institutionmemberrejectsubject'] = '기관 구성원 자격 요청이 거절되었습니다.';
$string['institutionmembership'] = '기관 구성원 자격';
$string['institutionmembershipdescription'] = '어느 기관의 구성원이면 여기에 표시될 것입니다. 기관의 구성원 자격 요청을 할 수도 있으며 기관이 가입하도록 초청한 경우 초청을 받아들이거나 거절할 수 있습니다.';
$string['institutionmembershipexpirywarning'] = '기관 구성원 자격 만료 경고';
$string['institutionmembershipexpirywarninghtml'] = '<p>안녕하세요? %s,</p> <p>구성원자격 %s (%s에서) %s에 만료됩니다..</p> <p>구성원 자격을 연장하거나 이에 대해 질문이 있으면 <a href="%s">연락처</a>로 연락주십시요.</p> <p>좋은 하루 되시길, %s 사이트관리자</p>';
$string['institutionmembershipexpirywarningtext'] = '안녕하세요? %s,구성원자격 %s (%s에서) %s에 만료됩니다. 구성원 자격을 연장하거나 이에 대해 질문이 있으면 %s로 연락주십시요. 좋은 하루 되시길. %s 사이트관리자';
$string['invalidcommunity'] = 'The community doesn\'t exist';
$string['invalidsesskey'] = '잘못된 세션 키';
$string['invitedgroup'] = '모둠이 초청된 곳:';
$string['invitedgroups'] = '모둠이 초청된 곳:';
$string['invitetocommunitymessage'] = '%s has invited you to join a community, \'%s\'.  Click on the link below for more information.';
$string['invitetocommunitysubject'] = 'You were invited to join a community';
$string['inviteuserfailed'] = 'Failed to invite the user';
$string['inviteusertojoincommunity'] = '커뮤티티에 이 사용자 초대';
$string['itemstaggedwith'] = '"%s"로 태그된 항목';
$string['javascriptnotenabled'] = '브라우저가 이 사이트에 대해 자바스크립트를 활성화하지 않은 것 같습니다. 마하라에 로그인하려면 자바스크립트를 활성화해야 합니다.';
$string['joincommunity'] = 'Join this community';
$string['joinedcommunity'] = 'You are now a community member';
$string['joininstitution'] = '기관에 가입';
$string['language'] = '언어';
$string['last'] = '마지막';
$string['lastminutes'] = '마지막 %s 분';
$string['lastmodified'] = '마지막 수정됨';
$string['lastname'] = '성';
$string['lastnamedescription'] = '성에 대한 설명';
$string['lastpage'] = '마지막 페이지';
$string['leavecommunity'] = 'Leave this community';
$string['leaveinstitution'] = '기관에서 탈퇴';
$string['leftcommunity'] = 'You have now left this community';
$string['leftcommunityfailed'] = 'Leaving community failed';
$string['linksandresources'] = '링크와 자원들';
$string['loading'] = '로딩 ...';
$string['loggedin'] = '사용자로 로그인 함';
$string['loggedinusersonly'] = '로그인한 사용자만';
$string['loggedoutok'] = '성공적으로 로그아웃 하였습니다';
$string['login'] = '로그인';
$string['loginfailed'] = '로그인 하기 위한 올바른 정보를 입력하지 않았습니다. 사용자 이름과 암호가 맞는지 확인하십시요.';
$string['loginto'] = '%s에 로그인';
$string['logout'] = '로그아웃';
$string['lostusernamepassword'] = '사용자이름/암호 분실';
$string['mainmenu'] = '메인 메뉴';
$string['makeprivate'] = '비공개로 변경';
$string['makepublic'] = '공개';
$string['member'] = '구성원';
$string['memberchangefailed'] = 'Failed to update some membership information';
$string['memberchangesuccess'] = 'Membership status changed successfully';
$string['membercount'] = '구성원 수';
$string['memberofinstitutions'] = '%s의 구성원';
$string['memberrequests'] = '가입 요청';
$string['members'] = '구성원들';
$string['membershipexpiry'] = '구성원 자격이 만료됩니다.';
$string['membershiptype'] = 'Community Membership Type';
$string['membershiptype.controlled'] = 'Controlled Membership';
$string['membershiptype.invite'] = '초청만';
$string['membershiptype.open'] = 'Open Membership';
$string['membershiptype.request'] = '가입 요청';
$string['message'] = '메세지';
$string['messagesent'] = '당신의 메세지가 보내어졌습니다.';
$string['months'] = '달';
$string['more...'] = '더 ...';
$string['mustspecifyoldpassword'] = '현재 암호를 입력하세요';
$string['myaddressbook'] = '내 주소록';
$string['mycommunities'] = '내 커뮤니티';
$string['mycontacts'] = '내 연락처';
$string['myfriends'] = '내 친구';
$string['mygroups'] = '내 모둠';
$string['myownedcommunities'] = '내가 만든 커뮤니티';
$string['myportfolio'] = '내 포트폴리오';
$string['mytags'] = '내 태그들';
$string['myviews'] = '내 보여주기';
$string['name'] = '이름';
$string['namedfieldempty'] = '필수항목 "%s" 이 비어있습니다';
$string['newpassword'] = '새 암호';
$string['next'] = '다음';
$string['nextpage'] = '다음 페이지';
$string['no'] = '아니오';
$string['nodeletepermission'] = '이 작품을 삭제할 권한이 없습니다.';
$string['noeditpermission'] = '이 작품을 편집할 권한이 없습니다.';
$string['noenddate'] = '종료일 없음';
$string['nohelpfound'] = '이 항목에 대한 도움말이 없습니다.';
$string['nohelpfoundpage'] = '이 페이지에 대한 도움말이 없습니다.';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>안녕하세요? %s,</p> 
<p>당신은 더이상 %s의 구성원이 아닙니다.</p> <p>계속 %s 를 현재의 사용자 이름  %s으로 사용할 수 있지만 계정에 대해 새 암호를 설정해야 합니다.</p> <p>암호를 재설정 하기 위해 아래 링크를 따라 가십시요.</p> <p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p> <p>이것에 대해 궁금한 것이 있으면 <a href="%scontact.php">연락</a> 주십시요.</p> <p>좋은 하루 되시길, %s 사이트 관리자</p> <p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['noinstitutionsetpassemailmessagetext'] = '안녕하세요? %s, 
당신은 더이상 %s의 구성원이 아닙니다. 계속 %s 를 현재의 사용자 이름  %s으로 사용할 수 있지만 계정에 대해 새 암호를 설정해야 합니다. 암호를 재설정 하기 위해 아래 링크를 따라 가십시요.%sforgotpass.php?key=%s 이것에 대해 궁금한 것이 있으면 %scontact.php 연락 주십시요.
좋은 하루 되시길, %s 사이트 관리자%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailsubject'] = '%s: %s의 구성원';
$string['none'] = '없음';
$string['nopublicfeedback'] = '아무 공개 댓글 없음';
$string['noresultsfound'] = '아무런 결과가 발견되지 않았습니다';
$string['nosendernamefound'] = '보내는 사람의 이름이 없습니다';
$string['nosessionreload'] = '로그인하기 위해 페이지를 다시 불러옴';
$string['nosuchpasswordrequest'] = '그러한 암호 요청이 없습니다.';
$string['notifications'] = '통지';
$string['notifysiteadministrator'] = '사이트 관리자에게 통지';
$string['notinstallable'] = '설치할 수 없음.';
$string['notinstalledplugins'] = '설치되지 않은 플러그인';
$string['notownerofview'] = 'You are not the owner of this view';
$string['notphpuploadedfile'] = '올리는 과정에서 파일이 손실되었습니다. 이러한 일은 발생하면 안됩니다. 자세한 것은 관리자에게 문의하십시요.';
$string['numitems'] = '%s 항목';
$string['oldpassword'] = '현재 암호';
$string['onlineusers'] = '온라인 사용자';
$string['optionalinstitutionid'] = '기관 ID(선택)';
$string['owner'] = '소유자';
$string['password'] = '암호';
$string['passwordchangedok'] = '암호가 성공적으로 변경되었습니다.';
$string['passworddescription'] = '암호에 대한 설명';
$string['passwordhelp'] = '이 시스템을 접근하는데 사용할 암호';
$string['passwordnotchanged'] = '암호를 변경하지 않았습니다, 새로운 암호를 선택하세요';
$string['passwordreminder'] = 'Password Reminder';
$string['passwordsaved'] = '새 암호가 저장되었습니다';
$string['passwordsdonotmatch'] = '암호가 틀립니다';
$string['passwordtooeasy'] = '암호가 너무 쉽습니다. 어려운 암호를 선택하세요';
$string['pendingfriend'] = '보류중인 친구';
$string['pendingfriends'] = '보류중인 친구들';
$string['pendingmembers'] = '보류된 구성원';
$string['phpuploaderror'] = '파일을 업로드 하는데 오류가 발생하였습니다: %s (오류 코드 %s)';
$string['phpuploaderror_1'] = '올리는 파일이 php.ini 파일에 설정된 upload_max_filesize 를 초과하였습니다.';
$string['phpuploaderror_2'] = '올리는 파일이 HTML 양식에 설정된 upload_max_filesize 를 초과하였습니다.';
$string['phpuploaderror_3'] = '업로드 파일이 부분적으로 업로드 되었습니다.';
$string['phpuploaderror_4'] = '파일이 업로드되지 않았습니다.';
$string['phpuploaderror_6'] = '임시 폴더 없음';
$string['phpuploaderror_7'] = '디스크에 파일쓰기 실패';
$string['phpuploaderror_8'] = '파일 업로드가 익스텐션에 의해 정지되었습니다.';
$string['placefeedback'] = '댓글 하기';
$string['pleasedonotreplytothismessage'] = '이 메세지에 답장을 보내지 마십시요.';
$string['plugindisabled'] = '플러그인 비활성화됨';
$string['pluginenabled'] = '플러그인 활성화됨';
$string['pluginnotenabled'] = '플러그인이 활성화 되지 않았습니다. 우선 %s 플러그인을 활성화 하십시요.';
$string['plugintype'] = '플러그인 타입';
$string['preferences'] = '선호사항';
$string['preferredname'] = '선호하는 이름';
$string['previous'] = '이전';
$string['prevpage'] = '이전 페이지';
$string['primaryemailinvalid'] = '이메일 주소가 잘못되었습니다.';
$string['print'] = 'Print';
$string['privacystatement'] = '개인정보 보호';
$string['private'] = '비공개';
$string['processing'] = '처리중';
$string['profile'] = '개인정보';
$string['profileicon'] = '프로파일 아이콘';
$string['profileimage'] = '개인정보 이미지';
$string['public'] = '공개';
$string['pwchangerequestsent'] = '계정의 암호를 변경하는데 사용할 수 있는 링크가 이메일로 보내어질 것입니다.';
$string['quarantinedirname'] = '검역';
$string['query'] = '질의';
$string['querydescription'] = '검색할 단어';
$string['quota'] = '할당량';
$string['quotausage'] = '사용 할당량 <span id="quota_used">%s</span> /총할당량 <span id="quota_total">%s</span>';
$string['reallyaddaccesstoemptyview'] = 'Your view contains no artefacts.  Do you really want to give these users access to the view?';
$string['reallyleaveinstitution'] = '이 기관을 탈퇴하기를 원하십니까?';
$string['reason'] = '이유';
$string['reasonoptional'] = '이유 (선택적)';
$string['recentupdates'] = '최근 업데이트';
$string['register'] = '등록';
$string['registeringdisallowed'] = '죄송합니다. 현재 이 시스템에 등록할 수 없습니다.';
$string['registerstep1description'] = 'Welcome! To use this site you must first register. You must also agree to the <a href="terms.php">terms and conditions</a>. The data we collect here will be stored according to our <a href="privacy.php">privacy statement</a>.';
$string['registerstep3fieldsmandatory'] = '<h3>Fill Out Mandatory Profile Fields</h3><p>The following fields are required. You must fill them out before your registration is complete.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Choose an Optional Profile Image</h3><p>You have now successfully registered with Mahara! You may now choose an optional profile icon to be displayed as your avatar. This image cannot be larger than 300x300 pixels.</p>';
$string['registrationcomplete'] = '%s 에 가입해 주셔서 감사합니다.';
$string['registrationnotallowed'] = '선택한 기관은 자체 등록을 허용하지 않습니다.';
$string['reject'] = '거절';
$string['rejectfriendshipreason'] = 'Reason for rejecting request';
$string['releaseview'] = 'Release view';
$string['remotehost'] = '원격 호스트 %s';
$string['remove'] = '제거';
$string['removedfromfriendslistmessage'] = '%s has removed you from their friends list.';
$string['removedfromfriendslistmessagereason'] = '%s has removed you from their friends list.  Their reason was:';
$string['removedfromfriendslistsubject'] = 'Removed from friends list';
$string['removefromfriendslist'] = 'Remove from friends';
$string['reportobjectionablematerial'] = 'Report objectionable material';
$string['reportsent'] = 'Your report has been sent';
$string['republish'] = '공개';
$string['request'] = '요청';
$string['requestedfriendlistmessage'] = '%s has requested that you add them as a friend.   You can either do this from the link below, or from your friends list page';
$string['requestedfriendlistmessagereason'] = '%s has requested that you add them as a friend. You can either do this from the link below, or from your friends list page. Their reason was:';
$string['requestedfriendlistsubject'] = '새 친구 요청';
$string['requestfriendship'] = 'Request friendship';
$string['requestjoincommunity'] = 'Request to join this community';
$string['requestmembershipofaninstitution'] = '이 기관에 대한 가입 요청';
$string['requiredfieldempty'] = '필수항목이 비어있습니다';
$string['result'] = '결과';
$string['results'] = '결과';
$string['returntosite'] = '사이트로 돌아가기';
$string['save'] = '저장';
$string['saveaccess'] = 'Save Access';
$string['savecommunity'] = '커뮤니티 저장';
$string['savegroup'] = '모둠 저장';
$string['search'] = '검색';
$string['searchresultsfor'] = '검색결과-검색어:';
$string['searchusers'] = '사용자 검색';
$string['select'] = '선택';
$string['selectatagtoedit'] = '편집할 태그 선택';
$string['selfsearch'] = '내 포트폴리오 검색';
$string['send'] = '보내기';
$string['sendinvitation'] = 'Send invite';
$string['sendmessage'] = '메세지 보내기';
$string['sendrequest'] = '요청 보냄';
$string['sessiontimedout'] = '세션이 종료되었습니다, 계속하기 위해서 로그인 정보를 입력하세요';
$string['sessiontimedoutpublic'] = '세션이 종료되었습니다. 계속 보기위해서는 <a href="?login">로그인</a> 하십시요.';
$string['sessiontimedoutreload'] = '세션이 종료되었습니다. 다시 로그인하기 위해 페이지를 다시 불러오십시요.';
$string['settings'] = '설정';
$string['settingssaved'] = '설정이 저장되었습니다';
$string['settingssavefailed'] = '설정 저장 실패';
$string['showtags'] = '내 태그 보이기';
$string['siteadministration'] = '사이트 관리';
$string['siteclosed'] = '사이트가 데이터베이스 업그레이드를 위해 일시적으로 닫혔습니다. 사이트 관리자는 로그인 할 수 있습니다.';
$string['siteclosedlogindisabled'] = '사이트가 데이터베이스 업그레이드를 위해 일시적으로 닫혔습니다.
<a href="%s">지금 업그레이드 하십시요.</a>';
$string['sitecontentnotfound'] = '%s 텍스트가 없습니다';
$string['size'] = '크기';
$string['sizeb'] = 'b';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'KB';
$string['sizemb'] = 'MB';
$string['sortalpha'] = '태그들을 알파벳으로 정렬';
$string['sortfreq'] = '빈도로 태그 정렬';
$string['sortresultsby'] = '결과 정렬- 방법:';
$string['strftimenotspecified'] = '명시되지 않음';
$string['studentid'] = 'ID 번호';
$string['subject'] = '제목';
$string['submit'] = '제출';
$string['submittedviews'] = 'Submitted views';
$string['submitview'] = 'Submit View';
$string['submitviewfailed'] = 'Submit view failed';
$string['submitviewquestion'] = 'If you submit this view for assessment, you will not be able to edit the view or any of its associated artefacts until your tutor has finished marking the view.  Are you sure you want to submit this view now?';
$string['system'] = '시스템';
$string['tagdeletedsuccessfully'] = '태그가 성공적으로 삭제되었습니다.';
$string['tagfilter_all'] = '모두';
$string['tagfilter_file'] = '파일';
$string['tagfilter_image'] = '이미지';
$string['tagfilter_text'] = '텍스트';
$string['tagfilter_view'] = '전시';
$string['tags'] = '태그';
$string['tagsdesc'] = '이 항목에 대해 콤마로 분리된 태그 입력';
$string['tagsdescprofile'] = '이 항목에 대해 콤마로 분리된 태그를 입력하세요. \'profile\'로 태그된 항목은 사이드바에 표시됩니다.';
$string['tagupdatedsuccessfully'] = '태그가 성공적으로 업데이트 되었습니다.';
$string['termsandconditions'] = '규정 및 조건';
$string['thisistheprofilepagefor'] = '%s 에 대한 개인정보 페이지 입니다.';
$string['title'] = '제목';
$string['tutor'] = '튜터';
$string['tutors'] = '튜터';
$string['type'] = '형식';
$string['unknownerror'] = '알수 없는 오류가 발생하였습니다 (0x20f91a0)';
$string['unreadmessage'] = '읽지 않은 메세지';
$string['unreadmessages'] = '읽지 않은 메세지';
$string['update'] = '새로고침';
$string['updatefailed'] = '새로고침 실패';
$string['updatemembership'] = 'Update membership';
$string['updatewatchlistfailed'] = '주시목록 갱신 실패';
$string['upload'] = '올리기';
$string['uploadedfiletoobig'] = '파일이 너무 큽니다. 자세한 것은 관리자에게 문의하십시요.';
$string['useradded'] = '사용자가 추가됨';
$string['useradministration'] = '사용자 관리';
$string['userdoesntwantfriends'] = 'This user doesn\'t want any new friends';
$string['userinvited'] = '초청장 보냈음';
$string['username'] = '사용자 이름';
$string['usernamedescription'] = '사용자 이름 설명';
$string['usernamehelp'] = '이 시스템을 접근하는데 주어진 사용자 이름';
$string['users'] = '사용자들';
$string['view'] = '보여주기';
$string['viewaccesseditedsuccessfully'] = 'View access saved successfully';
$string['viewavailable'] = 'View available';
$string['viewdeleted'] = 'View deleted';
$string['viewmyprofilepage'] = '개인정보 페이지 보기';
$string['viewreleasedmessage'] = 'The view that you submitted to community %s has been released back to you by %s';
$string['viewreleasedsubject'] = 'Your view has been released';
$string['viewreleasedsuccess'] = 'View was released successfully';
$string['views'] = '보여주기';
$string['viewsavailable'] = 'Views available';
$string['viewsubmitted'] = 'View submitted';
$string['viewsubmittedto'] = 'This view has been submitted to %s';
$string['virusfounduser'] = '올린 파일 %s을 바이러스 체크 프로그램으로 스캔해 본 결과 감염된 것으로 밝혀졌습니다. 파일 올리기를 못하였습니다.';
$string['virusrepeatmessage'] = '사용자 %s 가 여러개 파일을 올렸는데 바이러스 체크 프로그램으로 스캔해 본 결과 감염된 것으로 밝혀졌습니다.';
$string['virusrepeatsubject'] = '경고: %s 는 반복적으로 바이러스를 올리는 사람입니다..';
$string['watchlist'] = '내 주시목록';
$string['watchlistupdated'] = '당신의 주시목록이 갱신되었습니다';
$string['weeks'] = '주';
$string['width'] = '폭';
$string['widthshort'] = 'w';
$string['years'] = '년';
$string['yes'] = '예';
$string['youareamemberof'] = '당신은 %s의 구성원입니다.';
$string['youareloggedinas'] = '%s로 로그인 하였음';
$string['youaremasqueradingas'] = '당신은 %s로 가장하고 있습니다.';
$string['youhavebeeninvitedtojoin'] = '%s에 초대되었습니다.';
$string['youhavenottaggedanythingyet'] = '아무것도 태그한 것이 없습니다.';
$string['youhaverequestedmembershipof'] = '%s에 가입을 요청하였습니다.';
$string['youraccounthasbeensuspended'] = '당신의 계정이 사용중지 되었습니다';
$string['youraccounthasbeensuspendedreasontext'] = '%s 당신의 계정이 %s에 의해 사용중지 되었습니다. 사유:%s';
$string['youraccounthasbeensuspendedtext'] = '당신의 계정이 사용중지 되었습니다';
$string['youraccounthasbeensuspendedtext2'] = '%s 당신의 계정이 %s에 의해 사용중지 되었습니다.';
$string['youraccounthasbeenunsuspended'] = '당신의 계정을 사용할 수 있습니다';
$string['youraccounthasbeenunsuspendedtext'] = '당신의 계정을 사용할 수 있습니다';
$string['youraccounthasbeenunsuspendedtext2'] = '%s 당신의 계정이 %s에 의해 사용중지 해제 되었습니다. 다시 로그인 해서 사이트를 이용해 주십시요.';
$string['yournewpassword'] = '새 암호';
$string['yournewpasswordagain'] = '새 암호 다시 입력';
?>
