<?php
/**
 * This program is part of Mahara
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * @package    mahara
 * @subpackage lang
 * @author     Penny Leach <penny@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006,2007 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['memoryused'] = '메모리';
$string['timeused'] = '실행시간';
$string['seconds'] = '초';
$string['included'] = '포함된 파일들';
$string['dbqueries'] = 'DB 질의';
$string['reads'] = '읽기';
$string['writes'] = '쓰기';
$string['ticks'] = '틱';
$string['sys'] = '시스템';
$string['user'] = '사용자';
$string['cuser'] = '현재 사용자';
$string['csys'] = '현재 시스템';
$string['serverload'] = '서버 부하';


?>
