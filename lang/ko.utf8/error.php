<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = '접근 거절됨';
$string['accessdeniedexception'] = '이 페이지를 볼 수 있는 권한이 없습니다.';
$string['artefactnotfound'] = '아이디가 %s인 작품이 없습니다.';
$string['artefactnotfoundmaybedeleted'] = '아이디가 %s인 작품이 없습니다.(이미 삭제되었을 수 있음)';
$string['artefactpluginmethodmissing'] = '작품 플러그인%s는 %s를 구현해야 하는데, 그렇지 못합니다.';
$string['artefacttypeclassmissing'] = '작품 타입은 클래스를 구현해야 합니다. %s 가 없음.';
$string['artefacttypemismatch'] = '작품 타입 불일치. %s를 %s로 사용하려고 하고 있습니다.';
$string['artefacttypenametaken'] = '작품 타입(%s)은 이미 다른 플러그인(%s)이 사용하고 있습니다.';
$string['blockconfigdatacalledfromset'] = 'Configdata는 직접 설정해서는 안됩니다.  PluginBlocktype::instance_config_save 를 대신 사용하세요.';
$string['blockinstancednotfound'] = '아이디가 %s인 블록인스턴스가 없습니다.';
$string['blocktypelibmissing'] = '블록 %s (작품 플러그인%s 안의)에서 lib.php가 없음.';
$string['blocktypemissingconfigform'] = '블록 타입은 %s instance_config_form 를 구현해야 합니다.';
$string['blocktypenametaken'] = '블록 타입(%s)은 이미 다른 플러그인(%s)이 사용하고 있습니다.';
$string['blocktypeprovidedbyartefactnotinstallable'] = '이것은 작품 플러그인 %s 설치과정에서 설치됩니다.';
$string['classmissing'] = '클래스 %s (타입 %s 플러그인 %s) 이 없습니다';
$string['configsanityexception'] = '<p>It appears that your server\'s PHP configuration contains a setting that will prevent Mahara from working, or make your installation insecure. More details follow:</p><div id="reason">%s</div><p>Once you have made the appropriate changes, reload this page.</p>';
$string['couldnotmakedatadirectories'] = 'For some reason some of the core data directories could not be created. This should not happen, as Mahara previously detected that the dataroot directory was writable. Please check the permissions on the dataroot directory.';
$string['curllibrarynotinstalled'] = 'Your server configuration does not include the curl extension. Mahara requires this for Moodle integration and to retrieve external feeds. Please make sure that curl is loaded in php.ini, or install it if it is not installed.';
$string['datarootinsidedocroot'] = 'You have set up your data root to be inside your document root. This is a large security problem, as then anyone can directly request session data (in order to hijack other peoples\' sessions), or files that they are not allowed to access that other people have uploaded. Please configure the data root to be outside of the document root.';
$string['datarootnotwritable'] = 'Your defined data root directory, <tt>%s</tt>, is not writable. This means that neither session data, user files nor anything else that needs to be uploaded can be saved on your server. Please make the directory if it does not exist, or give ownership of the directory to the web server user if it does.';
$string['dbconnfailed'] = '<p>Mahara could not connect to the application database.</p>
<ul>
<li>If you are using Mahara, please wait a minute and try again</li>
<li>If you are the administrator, please check your database settings and make sure your database is available</li>
</ul>
<p>The error received was:</p>';
$string['dbextensionnotloaded'] = 'Your server configuration does not include either the pgsql or mysqli extension. Mahara requires one of these in order to store data in a relational database. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['dbnotutf8'] = 'You are not using a UTF-8 database. Mahara stores all data as UTF-8 internally. Please drop and re-create your database using UTF-8 encoding.';
$string['dbversioncheckfailed'] = 'Your database server version is not new enough to successfully run Mahara. Your server is %s %s, but Mahara requires at least version %s.';
$string['domextensionnotloaded'] = 'Your server configuration does not include the dom extension. Mahara requires this in order to parse XML data from a variety of sources.';
$string['gdextensionnotloaded'] = 'Your server configuration does not include the gd extension. Mahara requires this in order to perform resizes and other operations on uploaded images. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['gdfreetypenotloaded'] = 'Your server configuration of the gd extension does not include Freetype support. Mahara requires this to in order to build CAPTCHA images. Please make sure that gd is configured with it.';
$string['interactioninstancenotfound'] = '아이디가 %s인 활동 인스턴스가 없습니다.';
$string['invaliddirection'] = '잘못된 방향 %s';
$string['invalidviewaction'] = '잘못된 보여주기 제어 조치:%s';
$string['jsonextensionnotloaded'] = 'Your server configuration does not include the JSON extension. Mahara requires this in order to send some data to and from the browser. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['libxmlextensionnotloaded'] = 'Your server configuration does not include the libxml extension. Mahara requires this in order to parse XML data for the installer and for backups. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['magicquotesgpc'] = 'You have dangerous PHP settings, magic_quotes_gpc is on. Mahara is trying to work around this, but you should really fix it';
$string['magicquotesruntime'] = 'You have dangerous PHP settings, magic_quotes_runtime is on. Mahara is trying to work around this, but you should really fix it';
$string['magicquotessybase'] = 'You have dangerous PHP settings, magic_quotes_sybase is on. Mahara is trying to work around this, but you should really fix it';
$string['missingparamblocktype'] = '추가하기 위해 블록 타입을 선택해 보세요';
$string['missingparamcolumn'] = '컴럼 사양이 누락되었습니다.';
$string['missingparamid'] = '아이디가 없습니다.';
$string['missingparamorder'] = '순서 사양이 빠져 있습니다.';
$string['mysqldbextensionnotloaded'] = 'Your server configuration does not include the mysql extension. Mahara requires this in order to store data in a relational database. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['notartefactowner'] = '이 작품을 소유하고 있지 않습니다.';
$string['notfound'] = '없음';
$string['notfoundexception'] = '찾고자 하는 페이지가 없습니다.';
$string['onlyoneblocktypeperview'] = '보여주기에 한개의 %s 블록타입이상 추가할 수 없습니다.';
$string['onlyoneprofileviewallowed'] = '한개의 개인정보 보여주기만 허용됩니다.';
$string['parameterexception'] = '필수파라메터가 없습니다.';
$string['pgsqldbextensionnotloaded'] = 'Your server configuration does not include the pgsql extension. Mahara requires this in order to store data in a relational database. Please make sure that it is loaded in php.ini, or install it if it is not installed.
pgsqldbextensionnotloaded';
$string['phpversion'] = 'Mahara will not run on PHP < %s. Please upgrade your PHP version, or move Mahara to a different host.';
$string['registerglobals'] = 'You have dangerous PHP settings, register_globals is on. Mahara is trying to work around this, but you should really fix it';
$string['safemodeon'] = '<p>Your server appears to be running safe mode. Mahara does not support running in safe mode. You must turn this off in either the php.ini file, or in your apache config for the site.</p><p>If you are on shared hosting, it is likely that there is little you can do to get safe_mode turned off, other than ask your hosting provider. Perhaps you could consider moving to a different host.</p>';
$string['sessionextensionnotloaded'] = 'Your server configuration does not include the session extension. Mahara requires this in order to support users logging in. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['unknowndbtype'] = 'Your server configuration references an unknown database type. Valid values are "postgres8" and "mysql5". Please change the database type setting in config.php.';
$string['unrecoverableerror'] = 'A nonrecoverable error occurred.  This probably means that you have encountered a bug in the system.';
$string['unrecoverableerrortitle'] = '%s - 사이트가 없습니다.';
$string['versionphpmissing'] = '플러그인 %s %s는 version.php 가 없습니다.';
$string['viewnotfound'] = '아이디가 %s인 보여주기를 찾을 수 없습니다.';
$string['viewnotfoundexceptionmessage'] = '존재하지 않은 보여주기를 접근하려고 했습니다.';
$string['viewnotfoundexceptiontitle'] = '보여주기가 없습니다.';
$string['xmlextensionnotloaded'] = 'Your server configuration does not include the %s extension. Mahara requires this in order to parse XML data from a variety of sources. Please make sure that it is loaded in php.ini, or install it if it is not installed.';
$string['youcannotviewthisusersprofile'] = '이 사용자의 개인정보를 볼 수 없습니다.';
?>
