<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Body'] = '내용';
$string['Close'] = '닫기';
$string['Closed'] = '닫혀짐';
$string['Count'] = '카운트';
$string['Key'] = '키';
$string['Moderators'] = '중재자';
$string['Open'] = '열기';
$string['Order'] = '순서';
$string['Post'] = '게시';
$string['Poster'] = '게시자';
$string['Posts'] = '게시글';
$string['Reply'] = '답변';
$string['Sticky'] = '고정';
$string['Subject'] = '제목';
$string['Subscribe'] = '구독';
$string['Subscribed'] = '구독함';
$string['Topic'] = '토픽';
$string['Topics'] = '토픽';
$string['Unsticky'] = '유동';
$string['Unsubscribe'] = '구독해지';
$string['addpostsuccess'] = '게시글을 성공적으로 추가하였습니다.';
$string['addtitle'] = '포럼 추가';
$string['addtopic'] = '주제 추가';
$string['addtopicsuccess'] = '주제를 성공적으로 추가하였습니다.';
$string['autosubscribeusers'] = '자동으로 사용자들을 구독시킬까요?';
$string['autosubscribeusersdescription'] = '모둠 사용자들이 이 포럼을 자동으로 구독하도록 하게 할지 선택';
$string['cantaddposttoforum'] = '이 포럼에서 게시 권한이 없습니다.';
$string['cantaddposttotopic'] = '이 주제에서 게시 권한이 없습니다.';
$string['cantaddtopic'] = '이 포럼의 주제를 추가할 권한이 없습니다.';
$string['cantdeletepost'] = '이 포럼의 게시글을 삭제할 권한이 없습니다.';
$string['cantdeletethispost'] = '이 게시글을 삭제할 권한이 없습니다.';
$string['cantdeletetopic'] = '이 포럼의 주제를 삭제할 권한이 없습니다.';
$string['canteditpost'] = '이 게시글을 편집할 권한이 없습니다.';
$string['cantedittopic'] = '이 주제를 편집할 권한이 없습니다.';
$string['cantfindforum'] = '아이디가 %s 인 포럼을 찾을 수 없습니다.';
$string['cantfindpost'] = '아이디가 %s 인 게시글을 찾을 수 없습니다.';
$string['cantfindtopic'] = '아이디가 %s 인 주제를 찾을 수 없습니다.';
$string['cantviewforums'] = '이 모둠에서 포럼을 볼 권한이 없습니다.';
$string['cantviewtopic'] = '이 포럼에서 게시글을 볼 권한이 없습니다.';
$string['chooseanaction'] = '행동 선택';
$string['clicksetsubject'] = '주제를 설정하기 위해 클릭';
$string['closeddescription'] = '사회자와 모둠 관리자만 종료된 주제에 답글을 달 수 있습니다.';
$string['createtopicusersdescription'] = '"모든 모둠 구성원"으로 설정하면 누구든지 새 주제를 만들수 있고, 기존의 주제에 대해 답글을 올릴 수 있습니다. "사회자 및 모둠 관리자"로 하면 사회자나 모둠 관리자만 새 주제를 시작할 수 있습니다. 일단 주제가 게시되면 모든 사용자는 그 주제에 대한 답글을 올릴 수 있습니다.';
$string['currentmoderators'] = '현 사회자';
$string['defaultforumdescription'] = '%s 일반 토론 포럼';
$string['defaultforumtitle'] = '일반 토론';
$string['deleteforum'] = '포럼 삭제';
$string['deletepost'] = '게시글 삭제';
$string['deletepostsuccess'] = '게시글이 성공적으로 삭제되었습니다.';
$string['deletepostsure'] = '이것을 하기를 원하십니까? 되돌릴 수 없습니다.';
$string['deletetopic'] = '주제 삭제';
$string['deletetopicsuccess'] = '주제를 성공적으로 삭제하였습니다.';
$string['deletetopicsure'] = '이것을 하기를 원하십니까? 되돌릴 수 없습니다.';
$string['deletetopicvariable'] = '주제 \'%s\' 삭제';
$string['editpost'] = '게시글 편집';
$string['editpostsuccess'] = '게시글이 성공적으로 편집되었습니다.';
$string['editstothispost'] = '이 게시글에 대한 편집';
$string['edittitle'] = '포럼 편집';
$string['edittopic'] = '주제 편집';
$string['edittopicsuccess'] = '주제를 성공적으로 편집하였습니다.';
$string['forumname'] = '포럼 제목';
$string['forumposthtmltemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s by %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">답글 올리기 </a></p> <p><a href="%s"> 구독해지 %s</a></p> </div>';
$string['forumposttemplate'] = '%s : %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ 이 글을 온라인으로 보고 답글을 게시하려면 다음 링크를 클릭: %s. 구독해지 %s, 방문: %s';
$string['forumsuccessfulsubscribe'] = '포럼이 성공적으로 구독 되었습니다.';
$string['forumsuccessfulunsubscribe'] = '포럼이 성공적으로 구독해지 되었습니다.';
$string['gotoforums'] = '포럼으로 가기';
$string['groupadminlist'] = '모둠 관리자';
$string['groupadmins'] = '모둠 관리자';
$string['lastpost'] = '마지막 게시글';
$string['latestforumposts'] = '마지막 포럼 게시글';
$string['moderatorsandgroupadminsonly'] = '사회자와 모둠 관리자만';
$string['moderatorsdescription'] = '사회자는 주제나 게시글을 편집하거나 삭제할 수 있습니다. 또한 주제를 열거나 닫고, 고정으로 설정하거나 해지할 수 있습니다.';
$string['moderatorslist'] = '사회자';
$string['name'] = '포럼';
$string['nameplural'] = '포럼';
$string['newforum'] = '새 포럼';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = '새 게시글';
$string['newtopic'] = '새 주제';
$string['noforumpostsyet'] = '이 모둠에는 아직 게시글이 없습니다.';
$string['noforums'] = '이 모둠에는 아직 포럼이 없습니다.';
$string['notopics'] = '이 포럼에는 아직 주제가 없습니다.';
$string['orderdescription'] = '다른 포럼에 대해 정렬하고자 하는 포럼의 장소 선택';
$string['postbyuserwasdeleted'] = '%s가 게시한 글이 삭제되었습니다.';
$string['postdelay'] = '게시 지연';
$string['postdelaydescription'] = '포럼 사용자에게 새로운 게시글이 이메일 발송되기전에 지나야 하는 시간(분). 게시글 작성자는 이 시간안에 편집할 수 있습니다.';
$string['postedin'] = '%s가 %s에 게시됨';
$string['postreply'] = '답글 게시';
$string['postsvariable'] = '답글:%s';
$string['potentialmoderators'] = '잠재적 사회자';
$string['re'] = '회신:%s';
$string['regulartopics'] = '보통 주제들';
$string['replyforumpostnotificationsubject'] = '회신: %s: %s: %s';
$string['replyto'] = '답글:';
$string['stickydescription'] = '고정 주제는 모든 페이지의 상단에 있습니다.';
$string['stickytopics'] = '고정 주제';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = '포럼 구독';
$string['subscribetotopic'] = '주제 구독';
$string['today'] = '오늘';
$string['topicclosedsuccess'] = '주제가 성공적으로 종료되었습니다.';
$string['topicisclosed'] = '주제가 종료되었습니다. 사회자와 모둠 관리자만 새로운 답글을 게시할 수 있습니다.';
$string['topiclower'] = '주제';
$string['topicopenedsuccess'] = '주제가 성공적으로 열렸습니다.';
$string['topicslower'] = '주제들';
$string['topicstickysuccess'] = '주제가 성공적으로 고정 설정 되었습니다.';
$string['topicsubscribesuccess'] = '주제가 성공적으로 구독 되었습니다.';
$string['topicsuccessfulunsubscribe'] = '주제가 성공적으로 구독해지 되었습니다.';
$string['topicunstickysuccess'] = '주제가 성공적으로 고정 설정해지 되었습니다.';
$string['topicunsubscribesuccess'] = '주제가 성공적으로 구독해지 되었습니다.';
$string['topicupdatefailed'] = '주제 새로고침 실패';
$string['typenewpost'] = '세 포럼 게시글';
$string['unsubscribefromforum'] = '포럼 구독해지';
$string['unsubscribefromtopic'] = '주제 구독해지';
$string['updateselectedtopics'] = '선택된 주제를 새로고침';
$string['whocancreatetopics'] = '주제를 정할 수 있는 사람';
$string['yesterday'] = '어제';
$string['youarenotsubscribedtothisforum'] = '이 포럼을 구독하고 있지 않습니다.';
$string['youarenotsubscribedtothistopic'] = '이 주제를 구독하고 있지 않습니다.';
$string['youcannotunsubscribeotherusers'] = '다른 사용자들을 구독해지 시킬 수 없습니다.';
?>
