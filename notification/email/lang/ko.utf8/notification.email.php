<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['emailbody'] = '이것은 %s로 부터 자동으로 생성된 메세지입니다.  다음은 통지 내용입니다

--------------------------------------------------';
$string['emailbodyending'] = '통지 선택 사항을 갱신하기 위해서는 %s를 방문하세요';
$string['emailbodynoreply'] = '이것은 %s로 부터 자동으로 생성된 메세지입니다. 이 메세지에 답장하지 마세요.  다음은 통지 내용입니다

--------------------------------------------------';
$string['emailfooter'] = '%s 로 부터 자동으로 생성된 통지입니다. 통지 설정을 변경하기 위해서는 %s를 방문하세요.';
$string['emailheader'] = '%s 로 부터 통지가 왔습니다. 메세지는 다음과 같습니다:';
$string['emailsubject'] = '제목: %s';
$string['name'] = '이메일';
$string['referurl'] = '%s를 보세요';
?>
