<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['completeregistration'] = '등록 완료';
$string['description'] = '마하라 데이터베이스 사용 인증';
$string['emailalreadytaken'] = '이 이메일 주소는 이미 등록되었습니다.';
$string['iagreetothetermsandconditions'] = '나는 이 사이트의 규정과 조건에 동의합니다.';
$string['internal'] = '내부';
$string['passwordformdescription'] = '암호는 최소 6글자 이상이고 한개의 숫자와 2개 문자를 포함해야 합니다';
$string['passwordinvalidform'] = '암호는 최소 6개의 문자로 구성되어야 하며 한개 이상의 숫자와 2개 이상의 문자를 포함해야 합니다.';
$string['registeredemailmessagehtml'] = '<p>안녕하세요? %s,</p> <p> %s에 계정 등록해 주셔서 감사합니다. 등록절차를 마치기 위해서 다음 링크를 따라가세요:</p> <p><a href="http://folio.pcu.ac.kr/register.php?key=%s">http://folio.pcu.ac.kr/register.php?key=%s</a></p> <pre>-- 좋은 하루 되시길, %s 팀</pre>';
$string['registeredemailmessagetext'] = '안녕하세요? %s, Thank you for registering an account on %s. Please follow this link to complete the signup process:http://folio.pcu.ac.kr/register.php?key=%s -- 좋은 하루 되시길, %s 팀';
$string['registeredemailsubject'] = '%s에 등록되었습니다.';
$string['registeredok'] = '<p> 성공적으로 등록되었습니다. 계정을 활성화하기 위한 안내 메일을 확인하십시요.</p>';
$string['registrationnosuchkey'] = '죄송합니다. 이 키로는 등록이 되지 않습니다. 등록완료하기 위한 24시간이 지나갔는지 모릅니다. 그렇지 않은 경우 프로그램 오류입니다.';
$string['registrationunsuccessful'] = '죄송합니다. 등록이 안되었습니다. 이것은 우리 잘못입니다. 추후 다시 시도해 보십시요.';
$string['title'] = '내부';
$string['usernamealreadytaken'] = '죄송합니다. 이 사용자 이름은 이미 사용 중입니다';
$string['usernameinvalidform'] = '사용자 이름은 알파뉴메릭, 밑줄,점, @ 기호등을 포함할 수 있습니다. 사용자 이름은 3-30 문자 길이를 가질 수 있습니다.';
$string['youmaynotregisterwithouttandc'] = '규정과 조건을 준수하는데 동의하지 않으면 등록할 수 없습니다';
$string['youmustagreetothetermsandconditions'] = '<a href="terms.php">규정과 조건</a>에 동의해야 합니다';
?>
