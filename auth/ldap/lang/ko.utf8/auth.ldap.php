<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['contexts'] = '컨텍스트';
$string['description'] = 'LDAP 서버로 인증';
$string['distinguishedname'] = '구별된 이름';
$string['hosturl'] = '호스트 URL';
$string['ldapfieldforemail'] = '이메일 LDAP 항목';
$string['ldapfieldforfirstname'] = '이름 LDAP 항목';
$string['ldapfieldforsurname'] = '성 LDAP 항목';
$string['ldapversion'] = 'LDAP 버전';
$string['notusable'] = 'PHP LDAP 익스텐션을 설치하세요.';
$string['password'] = '암호';
$string['searchsubcontexts'] = '하위 컨텍스트 검색';
$string['title'] = 'LDAP';
$string['updateuserinfoonlogin'] = '로그인시 사용자 정보 갱신';
$string['userattribute'] = '사용자 속성';
$string['usertype'] = '사용자 타입';
$string['weautocreateusers'] = '자동으로 사용자를 생성합니다.';
?>
