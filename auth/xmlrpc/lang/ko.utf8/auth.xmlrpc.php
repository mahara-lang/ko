<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['description'] = '외부 어플리케이션의 SSO에 의해 인증';
$string['networkingdisabledonthissite'] = '네트워킹이 이 사이트에서 비활성화 되어 있습니다.';
$string['networkservers'] = '네트워크 서버';
$string['notusable'] = 'XMLRPC, Curl  및 OpenSSL PHP 익스텐션을 설치하세요.';
$string['title'] = 'XMLRPC';
$string['youhaveloggedinfrom'] = '<a href="%s">%s</a> 로부터 로그인 하였습니다.';
?>
