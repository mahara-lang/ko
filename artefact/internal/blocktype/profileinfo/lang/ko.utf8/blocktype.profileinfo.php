<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['aboutme'] = '내 소개';
$string['description'] = '표시할 개인정보를 선택';
$string['dontshowemail'] = '이메일 주소를 보여주시 마세요';
$string['dontshowprofileicon'] = '개인 프로파일 아이콘을 보여주지 마세요';
$string['fieldstoshow'] = '보여줄 항목';
$string['introtext'] = '소개 문장';
$string['title'] = '개인정보';
$string['uploadaprofileicon'] = '프로파일 아이콘이 없습니다. <a href="%sartefact/file/profileicons.php" target="_blank">프로파일아이콘 올리기</a>';
$string['useintroductioninstead'] = '개인소개 항목을 활성화하고 이 항목을 공백으로 남겨둔다음 개인소개 항목을 사용할 수 있습니다.';
?>
