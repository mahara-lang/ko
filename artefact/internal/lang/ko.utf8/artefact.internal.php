<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Created'] = '생성됨';
$string['Description'] = '설명';
$string['Download'] = '다운로드';
$string['Owner'] = '소유자';
$string['Preview'] = '미리보기';
$string['Size'] = '크기';
$string['Title'] = '제목';
$string['Type'] = '형식';
$string['aboutdescription'] = '실명으로 이름과 성을 입력하세요. 사람들에게 다른 이름을 보여주기를 원하면 그 이름을 선호하는 이름으로 하세요.';
$string['aboutme'] = '내 소개';
$string['address'] = '우편 주소';
$string['aimscreenname'] = '네이트온 아이디';
$string['backtoeditprofile'] = '신상명세 편집으로 돌아가기';
$string['blogaddress'] = '블로그 주소';
$string['businessnumber'] = '사무실 전화';
$string['city'] = '시/도';
$string['contact'] = '연락처 정보';
$string['contactdescription'] = '보여주기에 배치하기전까지 모든 정보는 비공개입니다.';
$string['country'] = '국가';
$string['default'] = '기본';
$string['editprofile'] = '개인정보 편집';
$string['editprofileicons'] = '신상명세 아이콘 편집';
$string['email'] = '이메일 주소 (복수 허용)';
$string['emailactivation'] = '이메일 활성화';
$string['emailactivationfailed'] = '이메일 활성화 실패';
$string['emailactivationsucceeded'] = '이메일 활성화 성공';
$string['emailaddress'] = '다른 이메일주소';
$string['emailalreadyactivated'] = '이메일이 이미 활성화 되었습니다.';
$string['emailingfailed'] = '개인정보가 저장되었습니다. 그러나 이메일은%s에게 보내지지 않았습니다';
$string['emailvalidation_body'] = '안녕하세요 %s,

이메일 주소 %s 가 Mahara의 당신 계정에 추가되었습니다. 이 주소를 활성화하기 위해서는 아래 링크를 방문하세요.

%s';
$string['emailvalidation_subject'] = '이메일 인증';
$string['faxnumber'] = '팩스 번호';
$string['firstname'] = '이름';
$string['fullname'] = '성과 이름';
$string['general'] = '일반';
$string['homenumber'] = '집 전화';
$string['icqnumber'] = 'ICQ 번호';
$string['industry'] = '산업';
$string['institution'] = '기관';
$string['introduction'] = '소개';
$string['invalidemailaddress'] = '잘못된 이메일 주소';
$string['jabberusername'] = '재버 사용자이름';
$string['lastmodified'] = '마지막 수정';
$string['lastname'] = '성';
$string['loseyourchanges'] = '변경사항을 잃어버렸나요?';
$string['mandatory'] = '필수적';
$string['messaging'] = '메세징';
$string['messagingdescription'] = '연락처 정보와 마찬가지로 이 정보는 비공개입니다.';
$string['mobilenumber'] = '이동 전화';
$string['msnnumber'] = 'MSN 채팅';
$string['myfiles'] = '내 파일들';
$string['myprofile'] = '내 신상명세';
$string['name'] = '이름';
$string['noimagesfound'] = '아무 이미지도 발견되지 않았습니다';
$string['occupation'] = '직업';
$string['officialwebsite'] = '공식 웹사이트 주소';
$string['onlyfiveprofileicons'] = '5개까지 신상명세 아이콘을 업로드 할 수 있습니다';
$string['personalwebsite'] = '개인 웹사이트 주소';
$string['pluginname'] = '개인정보';
$string['preferredname'] = '원하는 이름';
$string['principalemailaddress'] = '주요 이메일주소';
$string['profile'] = '개인정보';
$string['profilefailedsaved'] = '개인정보 저장 실패';
$string['profileicon'] = '신상명세 아이콘';
$string['profileiconimagetoobig'] = '올리기한 이미지가 너무 큽니다(%sx%s 픽셀). 크기는 300x300 픽셀을 초과해서는 안됩니다';
$string['profileicons'] = '신상명세 아이콘';
$string['profileiconsdefaultsetsuccessfully'] = '기본 신상명세 아이콘이 성공적으로 설정되었습니다';
$string['profileiconsdeletedsuccessfully'] = '신상명세 아이콘이 성공적으로 삭제되었습니다';
$string['profileiconsetdefaultnotvalid'] = '기본 신상명세 아이콘을 설정할 수 없었습니다. 잘 못 선택하였습니다';
$string['profileiconsiconsizenotice'] = '여기서 <strong>5개</strong>의 신상명세 아이콘을 업로드 할 수 있습니다. 기본 아이콘으로 사용할 하나를 선택하세요. 아이콘의 크기는 100x100 에서 300x300 픽셀까지 가능합니다';
$string['profileiconsize'] = '아이콘 크기';
$string['profileiconsnoneselected'] = '삭제할 신상명세 아이콘이 선택되지 않았습니다';
$string['profileiconuploadexceedsquota'] = '이 신상명세 아이콘을 업로드하게 되면 당신의 디스크 할당량을 초과하게 할 것입니다. 업로드한 파일을 삭제해 보십시요';
$string['profileinformation'] = '개인정보';
$string['profilepage'] = '개인정보 페이지';
$string['profilesaved'] = '개인정보가 성공적으로 저장되었습니다';
$string['public'] = '공개';
$string['saveprofile'] = '개인정보 저장';
$string['skypeusername'] = '스카이프 사용자이름';
$string['studentid'] = '학생 ID';
$string['town'] = '동,읍';
$string['unvalidatedemailalreadytaken'] = '당신이 인증하려고 하는 이메일 주소는 이미 사용 중입니다';
$string['uploadedprofileiconsuccessfully'] = '새 신상명세 아이콘을 성공적으로 업로드하였습니다';
$string['uploadprofileicon'] = '신상명세 아이콘 업로드';
$string['validationemailsent'] = '확인 이메일이 보내어졌습니다.';
$string['validationemailwillbesent'] = '개인정보를 저장할때 확인 이메일이 보내어집니다.';
$string['verificationlinkexpired'] = '확인 링크가 만료되었습니다';
$string['viewallprofileinformation'] = '모든 개인정보 보기';
$string['viewmyprofile'] = '내 개인정보 보기';
$string['viewprofilepage'] = '개인정보 페이지 보기';
$string['yahoochat'] = '야후 채팅';
?>
