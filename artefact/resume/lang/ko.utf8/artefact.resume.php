<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['academicgoal'] = '학문적 목표';
$string['academicskill'] = '학문적 기술';
$string['backtoresume'] = '내 이력서로 돌아가기';
$string['book'] = '저술';
$string['careergoal'] = '이력 목표';
$string['certification'] = '자격증, 인증서, 수상 경력';
$string['citizenship'] = '국적';
$string['compositedeleteconfirm'] = '이것을 삭제하기를 원하십니까?';
$string['compositedeleted'] = '성공적으로 삭제하였습니다';
$string['compositesaved'] = '성공적으로 저장하였습니다';
$string['compositesavefailed'] = '저장 실패';
$string['confirmeditprofile'] = '당신의 신상명세를 계속 편집하면, 이 영역에서 저장되지 않은 정보는 잃어버리게 될 것 입니다';
$string['contactinformation'] = '연락 정보';
$string['contribution'] = '기여';
$string['coverletter'] = '겉 편지';
$string['current'] = '현재';
$string['date'] = '날자';
$string['dateofbirth'] = '생년월일';
$string['description'] = '설명';
$string['detailsofyourcontribution'] = '기여 세부사항';
$string['educationhistory'] = '학력';
$string['employer'] = '고용자';
$string['employmenthistory'] = '경력';
$string['enddate'] = '종료일';
$string['female'] = '여성';
$string['gender'] = '성';
$string['goalandskillsaved'] = '성공적으로 저장하였습니다';
$string['institution'] = '기관';
$string['interest'] = '관심사';
$string['jobdescription'] = '직책 설명';
$string['jobtitle'] = '직위';
$string['male'] = '남성';
$string['maritalstatus'] = '결혼 상태';
$string['membership'] = '전문 멤버쉽';
$string['movedown'] = '아래로 옮김';
$string['moveup'] = '위로 옮김';
$string['mygoals'] = '내 목표';
$string['myresume'] = '내 이력서';
$string['myskills'] = '내 기술들';
$string['personalgoal'] = '개인 목표';
$string['personalinformation'] = '개인 정보';
$string['personalskill'] = '개인 기술들';
$string['placeofbirth'] = '출생지';
$string['pluginname'] = '이력서';
$string['position'] = '직책';
$string['profilegotoresume'] = '내 이력서 편집';
$string['qualdescription'] = '자격 설명';
$string['qualification'] = '자격';
$string['qualname'] = '자격 이름';
$string['qualtype'] = '자격 형식';
$string['resume'] = '이력서';
$string['resumeofuser'] = '%s의 이력서';
$string['resumesaved'] = '이력서가 저장됨';
$string['resumesavefailed'] = '이력서를 새로 고침하는데 실패하였습니다';
$string['startdate'] = '시작일';
$string['title'] = '직위';
$string['viewyourresume'] = '자신의 이력서 보기';
$string['visastatus'] = '비자 상태';
$string['workskill'] = '직업 기술';
?>
