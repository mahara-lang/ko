<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['absolutebottom'] = '절대 아래';
$string['absolutemiddle'] = '절대 중간';
$string['addblog'] = '블로그 추가';
$string['addpost'] = '글 추가';
$string['alignment'] = '정렬';
$string['attach'] = '첨부';
$string['attachedfilelistloaded'] = '첨부된 파일 목록이 로드되었습니다';
$string['attachedfiles'] = '첨부된 파일들';
$string['attachments'] = '첨부';
$string['baseline'] = '바닥선';
$string['blog'] = '블로그';
$string['blogcopiedfromanotherview'] = '이 블록은 다른 보여주기에서 복사되었습니다. 이 블록을 옮기고 제거할 수 있지만 그 안에 있는 것을 변경할 수 없습니다.';
$string['blogdeleted'] = '블로그가 삭제되었습니다';
$string['blogdesc'] = '설명';
$string['blogdescdesc'] = '예: ‘유순의 경험과 고찰들에 대한 기록’';
$string['blogdoesnotexist'] = '존재하지 않는 블로그입니다.';
$string['blogfilesdirdescription'] = '파일들이 블로그 게시물의 첨부파일로 업로드 되었습니다';
$string['blogfilesdirname'] = '블로그파일들';
$string['blogpost'] = '블로그 게시물';
$string['blogpostdeleted'] = '블로그 게시물이 삭제되었습니다';
$string['blogpostdoesnotexist'] = '존재하지 않는 블로그 글 입니다.';
$string['blogpostpublished'] = '블로그 게시물이 공개되었습니다';
$string['blogpostsaved'] = '블로그 게시물이 저장되었습니다';
$string['blogs'] = '블로그';
$string['blogsettings'] = '블로그 설정들';
$string['blogtitle'] = '제목';
$string['blogtitledesc'] = '예: ‘유순의 간호 실습 저널’.';
$string['border'] = '경계선';
$string['bottom'] = '아래';
$string['browsemyfiles'] = '내 파일들 살펴보기';
$string['cancel'] = '취소';
$string['cannotdeleteblogpost'] = '이 블로그 게시물을 제거하는데 오류가 발생하였습니다';
$string['commentsallowed'] = '댓글';
$string['commentsalloweddesc'] = '댓글 설명';
$string['commentsallowedno'] = '이 게시물에 대한 댓글 허용 안함';
$string['commentsallowedyes'] = '로그인 한 사용자에게 이 게시물에 대한 댓글 허용';
$string['commentsnotify'] = '댓글 통지';
$string['commentsnotifydesc'] = '다른 사람이 당신의 게시물에 답글을 할때마다 선택적으로 통지를 받을 수 있습니다.';
$string['commentsnotifyno'] = '이 블로그에 대한 답글에 대해 나에게 통지하지 마십시요';
$string['commentsnotifyyes'] = '이 블로그에 대한 답글에 대해 나에게 알려주시오';
$string['copyfull'] = '다른 사람들은 %s의 복사본을 갖게 됩니다.';
$string['copynocopy'] = '보여주기를 복사할 때 이 블록을 완전히 생략';
$string['copyreference'] = '다른 사람들이 그들의 보여주기에서 당신의 %s를 표시할 수도 있습니다.';
$string['createandpublishdesc'] = '블로그 게시물을 만들고 다른 사람에게 공개할 것입니다.';
$string['createasdraftdesc'] = '블로그 게시물을 만들지만 공개하기 전까지는 다른사람들이 볼 수 없습니다.';
$string['createblog'] = '블로그 만들기';
$string['dataimportedfrom'] = '%s에서 가져온 데이터';
$string['defaultblogtitle'] = '%s의 블로그';
$string['delete'] = '삭제';
$string['deleteblog?'] = '이 블로그를 삭제하시기를 원하십니까?';
$string['deleteblogpost?'] = '이 게시물을 삭제하시기를 원하십니까?';
$string['description'] = '설명';
$string['dimensions'] = '차원들';
$string['draft'] = '초안';
$string['edit'] = '편집';
$string['editblogpost'] = '블로그 게시물 편집';
$string['entriesimportedfromleapexport'] = '다른 곳에서 가져오기 할 수 없었던, LEAP내보내기에서 가져오기한 항목들';
$string['erroraccessingblogfilesfolder'] = '블로그 파일폴더 접근오류';
$string['errorsavingattachments'] = '블로그 게시물 첨부파일을 저장하는데 오류가 발생하였습니다';
$string['horizontalspace'] = '수평 공간';
$string['insert'] = '삽입';
$string['insertimage'] = '이미지 삽입';
$string['left'] = '왼쪽';
$string['middle'] = '중간';
$string['moreoptions'] = '추가 옵션';
$string['mustspecifycontent'] = '게시글에 내용을 입력하세요.';
$string['mustspecifytitle'] = '게시글에 제목을 입력하세요.';
$string['myblogs'] = '내 블로그';
$string['name'] = '이름';
$string['newattachmentsexceedquota'] = '이 게시물에 업로드한 새 파일의 크기가 당신의 할당량을 초과하게 만들 수 있습니다. 방금 추가한 첨부파일들을 제거하면 게시물을 저장할 수 있을 것입니다.';
$string['newblog'] = '새 블로그';
$string['newblogpost'] = '블로그 "%s" 에 새 게시글';
$string['newerposts'] = '새로운 게시글';
$string['nofilesattachedtothispost'] = '첨부파일 없음';
$string['noimageshavebeenattachedtothispost'] = '이 게시물에 이미지가 첨부되지 않았습니다. 이미지를 삽입하기 위해서는 이미지를 업로드하거나 이미지를 게시물에 추가해야 합니다.';
$string['nopostsaddone'] = '아직 아무 게시글이 없습니다. %s게시글 추가%s!';
$string['noresults'] = '아무 결과도 없음';
$string['olderposts'] = '다른 게시글들';
$string['pluginname'] = '블로그';
$string['postbody'] = '게시내용';
$string['postbodydesc'] = '게시설명';
$string['postedbyon'] = '게시자 %s 개시일 %s';
$string['postedon'] = '게시일';
$string['posts'] = '게시글';
$string['postscopiedfromview'] = '%s 에서 복사된 게시글';
$string['posttitle'] = '제목';
$string['posttitledesc'] = '제목은 게시물 위에 나타납니다.';
$string['publish'] = '공개';
$string['publishblogpost?'] = '이 게시물을 공개하기를 원하십니까?';
$string['published'] = '공개됨';
$string['publishfailed'] = '오류가 발생하였습니다.  게시물이 공개되지 않았습니다';
$string['remove'] = '제거';
$string['right'] = '오른쪽';
$string['save'] = '저장';
$string['saveandpublish'] = '저장 및 공개';
$string['saveasdraft'] = '초안으로 저장';
$string['savepost'] = '게시물 저장';
$string['savesettings'] = '설정 저장';
$string['settings'] = '설정들';
$string['textbottom'] = '문장 아래';
$string['texttop'] = '문장 위';
$string['thisisdraft'] = '이 게시물은 초안입니다';
$string['thisisdraftdesc'] = '게시물이 초안이면 당신외에는 아무도 볼 수가 없습니다';
$string['title'] = '제목';
$string['top'] = '위';
$string['update'] = '새로고침';
$string['verticalspace'] = '수직 공간';
$string['viewblog'] = '블로그 보기';
$string['viewposts'] = '복사된 글(%s)';
$string['youarenottheownerofthisblog'] = '이 블로그의 소유자가 아닙니다';
$string['youarenottheownerofthisblogpost'] = '이 블로그 게시물의 소유자가 아닙니다';
$string['youhaveblogs'] = '%s의 블로그가 있습니다.';
$string['youhavenoblogs'] = '블로그가 없습니다.';
$string['youhaveoneblog'] = '1개 블로그가 있습니다.';
?>
