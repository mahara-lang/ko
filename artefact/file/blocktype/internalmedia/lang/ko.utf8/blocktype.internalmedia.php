<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['configdesc'] = '이 블록에 임베드할 수 있는 파일 형식을 선택하세요. 파일 아티팩트 플러그인에 의해 비활성화된 형식은 사용할 수 없습니다. 이미 블록에서 사용되고 있는 파일 형식을 비활성화 하면 더 이상 렌더되지 않습니다.';
$string['description'] = '임베드된 보기를 위해 파일을 선택';
$string['flashanimation'] = '플래시 애니메이션';
$string['media'] = '미디어';
$string['title'] = '엠베디드 미디어';
$string['typeremoved'] = '이 블록은 관리자에 의해 허용되지 않은 미디어 파일 형식을 가르킵니다.';
?>
