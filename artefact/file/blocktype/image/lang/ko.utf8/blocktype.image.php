<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['description'] = '파일에서 한개 이미지';
$string['showdescription'] = '설명을 보여줄까요?';
$string['title'] = '이미지';
$string['width'] = '폭';
$string['widthdescription'] = '이미지의 폭을 (픽셀로) 표시하세요. 이미지는 이 폭으로 맞추어 집니다. 이미지 원래 크기를 사용하려면 공백으로 남겨 놓으세요.';
?>
