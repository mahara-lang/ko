<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Contents'] = '콘텐츠';
$string['Continue'] = '계속';
$string['Created'] = '생성됨';
$string['Date'] = '일자';
$string['Default'] = '기본';
$string['Description'] = '설명';
$string['Details'] = '세부사항';
$string['Download'] = '다운로드';
$string['File'] = '파일';
$string['Files'] = '파일';
$string['Folder'] = '폴더';
$string['Folders'] = '폴더';
$string['Name'] = '이름';
$string['Owner'] = '소유자';
$string['Preview'] = '미리보기';
$string['Size'] = '크기';
$string['Title'] = '제목';
$string['Type'] = '형식';
$string['Unzip'] = '압축풀기';
$string['addafile'] = '파일 추가';
$string['adminfilesloaded'] = '관리 파일들이 로드됨';
$string['ai'] = '포스트스크립트 문서';
$string['aiff'] = 'AIFF 오디오 파일';
$string['application'] = '알수 없는 응용프로그램';
$string['archive'] = '아카이브';
$string['au'] = 'AU 오디오 파일';
$string['avi'] = 'AVI 비디오 파일';
$string['bmp'] = 'Bitmap 이미지';
$string['bytes'] = '바이트';
$string['bz2'] = 'Bzip2 압축 파일';
$string['cannoteditfolder'] = '이 폴더에 콘텐츠를 추가할 권한이 없습니다.';
$string['cantcreatetempprofileiconfile'] = '%s 에 임시 개인정보 아이콘 이미지를 저장할 수 없습니다.';
$string['changessaved'] = '변경이 저장됨';
$string['clickanddragtomovefile'] = '%s를 옮기기 위해 클릭하고 드래그 하세요';
$string['confirmdeletefile'] = '이 파일을 삭제하기를 원하십니까?';
$string['confirmdeletefolder'] = '이 폴더를 삭제하기를 원하십니까?';
$string['confirmdeletefolderandcontents'] = '이 폴더와 그 안에 있는 콘텐츠 모두들 삭제하기를 원하십니까?';
$string['contents'] = '콘텐츠';
$string['copyrightnotice'] = '저작권 알림';
$string['create'] = '만들기';
$string['createfolder'] = '폴더 만들기';
$string['customagreement'] = '맞춤 동의';
$string['defaultagreement'] = '기본 동의';
$string['defaultquota'] = '기본 할당량';
$string['defaultquotadescription'] = '새 사용자들에 대한 디스크 할당량을 여기서 설정할 수 있습니다. 기존 사용자의 할당량은 변경되지 않습니다.';
$string['deletefile?'] = '이 파일을 삭제하는 것이 확실합니까?';
$string['deletefolder?'] = '이 폴더를 삭제하는 것이 확실합니까?';
$string['deleteselectedicons'] = '선택된 아이콘 삭제';
$string['description'] = '설명';
$string['destination'] = '설명';
$string['doc'] = 'MS Word 문서';
$string['download'] = '다운로드';
$string['downloadfile'] = '%s 를 다운로드';
$string['downloadoriginalversion'] = '원래 버전을 다운로드';
$string['dss'] = '디지탈 Speech 표준 사운드 파일';
$string['editfile'] = '파일 편집';
$string['editfolder'] = '폴더 편집';
$string['emptyfolder'] = '빈 폴더';
$string['extractfilessuccess'] = '%s 폴더 및 %s 파일 생성.';
$string['file'] = '파일';
$string['filealreadyindestination'] = '옮긴 파일이 이미 그 폴더에 있습니다.';
$string['fileappearsinviews'] = '이 파일은 한두개 이상의 보여주기에 나타납니다.';
$string['fileattached'] = '이 파일은 이포트폴리오에서 %s 다른 항목에 첨부되어 있습니다.';
$string['fileexists'] = '파일이 존재합니다';
$string['fileexistsonserver'] = '같은 이름의 파일 %s 이 이미 존재합니다.';
$string['fileexistsoverwritecancel'] = '같은 이름의 파일이 이미 존재합니다.  다른 이름을 사용하거나 기존 파일을 덮어쓰기 할 수 있습니다.';
$string['fileinstructions'] = '보여주기에 포함될 이미지, 문서, 혹은 다른 파일을 올리세요. 파일이나 폴더를 이동하시 위해서는 원하는 폴더에 드래그 드롭하세요.';
$string['filelistloaded'] = '파일 목록이 로드되었습니다';
$string['filemoved'] = '파일을 성공적으로 이동되었습니다.';
$string['filenamefieldisrequired'] = '파일 항목이 필요합니다';
$string['files'] = '파일들';
$string['filesextractedfromarchive'] = '아카이브에서 가져온 파일들';
$string['filesextractedfromziparchive'] = 'Zip 아카이브에서 가져온 파일들';
$string['fileswillbeextractedintofolder'] = '파일이 %s에 풀어질 것입니다.';
$string['filethingdeleted'] = '%s 삭제됨';
$string['filetypedescription'] = '<p>사용자들이 올리기 할 수 있는 허용된 파일 형식을 여기서 설정할 수 있습니다. 이것을 통하여 올리기 할 수 있는 것들을 통제하게 해 줍니다. 만일 바이러스 검사옵션이 켜져있는 경우, 바이러스 검사외에 형식 검사가 수행될 것입니다.</p><p> 어떤 무비나 gzip 과 같은 압축파일이 제대로 동작하기 위해서는 &quot;Unknown Application&quot; 가 필요할 수도 있습니다.<p>';
$string['filetypes'] = '올리기 가능한 파일 형식 설정';
$string['fileuploadedas'] = '%s 가 "%s" 로 올려짐';
$string['fileuploadedtofolderas'] = '%s 가 %s 에 "%s" 로 올려짐';
$string['filewithnameexists'] = '"%s" 라는 이름의 파일 혹은 폴더가 이미 존재합니다.';
$string['flv'] = 'FLV 플래시 무비';
$string['folder'] = '폴더';
$string['folderappearsinviews'] = '이 폴더는 한개 이상의 보여주기에 표시됩니다.';
$string['foldercreated'] = '폴더가 만들어졌습니다';
$string['foldernamerequired'] = '새 폴더이름을 입력하세요';
$string['foldernotempty'] = '이 폴더는 비어있지 않습니다.';
$string['gif'] = 'GIF 이미지';
$string['gotofolder'] = '%s 로 가기';
$string['groupfiles'] = '모둠 파일';
$string['gz'] = 'Gzip 압축 파일';
$string['home'] = '홈';
$string['html'] = 'HTML 파일';
$string['htmlremovedmessage'] = '<strong>%s</strong> (소유자: <a href="%s">%s</a>)를 보고 있습니다. 아래에 보여진 파일은 유해 콘텐츠를 제거하기 위해 필터된 것으로 원본과 약간 다를 수 있습니다.';
$string['htmlremovedmessagenoowner'] = '<strong>%s</strong>를 보고 있습니다. 아래에 보여진 파일은 유해 콘텐츠를 제거하기 위해 필터된 것으로 원본과 약간 다를 수 있습니다.';
$string['image'] = '이미지';
$string['imagetitle'] = '이미지 제목';
$string['insufficientquotaforunzip'] = '남아있는 파일 할당량이 이 파일을 풀어놓기에는 너무 적습니다.';
$string['invalidarchive'] = '아카이브 파일을 읽는데 오류';
$string['jpeg'] = 'JPEG 이미지';
$string['jpg'] = 'jpeg JPEG 이미지';
$string['js'] = 'Javascript 파일';
$string['lastmodified'] = '최종 수정됨';
$string['latex'] = 'LaTeX 문서';
$string['m3u'] = 'M3U 오디오 파일';
$string['maxuploadsize'] = '최대 올리기 파일 크기';
$string['mov'] = 'MOV 퀵타임 무비';
$string['movefailed'] = '이동 실패';
$string['movefaileddestinationinartefact'] = '폴더를 같은 폴더안에 놓을 수 없습니다.';
$string['movefaileddestinationnotfolder'] = '폴더안으로 파일들을 옮길 수 있습니다.';
$string['movefailednotfileartefact'] = '파일, 폴더 및 이미지 작품이 이동될 수 있습니다.';
$string['movefailednotowner'] = '이 폴더로 파일을 옮길 수 있는 권한이 없습니다.';
$string['mp3'] = 'MP3 오디오 파일';
$string['mp4'] = 'MP4 오디오 파일';
$string['mp4_audio'] = 'MP4 오디오파일';
$string['mp4_video'] = 'MP4 비디오파일';
$string['mpeg'] = 'MPEG 무비';
$string['mpg'] = 'MPG 무비';
$string['myfiles'] = '내 파일들';
$string['name'] = '이름';
$string['namefieldisrequired'] = '이름 항목이 필요합니다';
$string['nametoolong'] = '이름이 너무 깁니다. 간단한 것을 선택하세요.';
$string['nofilesfound'] = '아무 파일도 발견되지 않았습니다';
$string['noimagesfound'] = '이미지 파일이 없습니다.';
$string['notpublishable'] = '이 파일을 공개할 권한이 없습니다.';
$string['odb'] = 'Openoffice 데이터베이스';
$string['odc'] = 'Openoffice Calc 파일';
$string['odf'] = 'Openoffice Formula 파일';
$string['odg'] = 'Openoffice 그래픽스 파일';
$string['odi'] = 'Openoffice 이미지';
$string['odm'] = 'Openoffice 마스터 문서 파일';
$string['odp'] = 'Openoffice 프리젠테이션';
$string['ods'] = 'Openoffice 스프레드시트';
$string['odt'] = 'Openoffice 문서';
$string['onlyfiveprofileicons'] = '5개의 개인정보 아이콘을 올릴 수 있습니다.';
$string['or'] = '또는';
$string['oth'] = 'Openoffice 웹 문서';
$string['ott'] = 'Openoffice 템플릿 문서';
$string['overwrite'] = '덮어쓰기';
$string['parentfolder'] = '상위 폴더';
$string['pdf'] = 'PDF 문서';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = '파일 압축이 풀어지는 동안 기다리세요.';
$string['pluginname'] = '파일';
$string['png'] = 'PNG 이미지';
$string['ppt'] = 'MS 파워포인트 문서';
$string['profileicon'] = '개인정보 아이콘';
$string['profileiconimagetoobig'] = '올린 이미지가 너무 큽니다(%sx%s 픽셀). %sx%s 픽셀 이하여만 합니다.';
$string['profileicons'] = '개인정보 아이콘';
$string['profileiconsdefaultsetsuccessfully'] = '기본 개인정보 아이콘이 성공적으로 설정되었습니다.';
$string['profileiconsdeletedsuccessfully'] = '개인정보 아이콘이 성공적으로 삭제되었습니다.';
$string['profileiconsetdefaultnotvalid'] = '기본 개인정보 아이콘을 설정할 수 없습니다. 잘못된 선택입니다.';
$string['profileiconsiconsizenotice'] = '<strong>5개</strong>까지 개인정보 아이콘을 올릴 수 있습니다. 기본 아이콘으로 사용할 것을 선택하세요. 아이콘 크기는 16x16 과 %sx%s 픽셀 사이이어야 합니다.';
$string['profileiconsize'] = '개인정보 아이콘  크기';
$string['profileiconsnoneselected'] = '삭제할 개인정보 아이콘이 선택되지 않았습니다.';
$string['profileiconuploadexceedsquota'] = '이 개인정보 아이콘을 올리면 디스크 할당량이 초과됩니다. 올린 파일 몇개를 삭제하십시요.';
$string['quicktime'] = 'Quicktime 무비';
$string['ra'] = 'Real 오디오 파일';
$string['ram'] = 'RAM 리얼 플레이어 무비';
$string['requireagreement'] = '동의가 필요합니다.';
$string['rm'] = 'RM 리얼 플레이어 무비';
$string['rpm'] = 'RPM 리얼 플레이어 무비';
$string['rtf'] = 'RTF 문서';
$string['savechanges'] = '변경 저장';
$string['selectafile'] = '파일 선택';
$string['setdefault'] = '기본으로 설정';
$string['sgi_movie'] = 'SGI 무비 파일';
$string['sh'] = 'Shell 스크립트';
$string['sitefilesloaded'] = '사이트 파일이 로드됨';
$string['size'] = '크기';
$string['spacerequired'] = '공간이 필요함';
$string['spaceused'] = '공간이 사용됨';
$string['swf'] = 'swf 플래시 무비';
$string['tar'] = 'TAR 아카이브';
$string['timeouterror'] = '파일 올리기가 실패하였습니다. 파일을 다시 올리려고 합니다.';
$string['title'] = '이름';
$string['titlefieldisrequired'] = '제목 항목이 필요합니다';
$string['txt'] = 'Plain 텍스트 파일';
$string['unlinkthisfilefromblogposts?'] = '이 파일은 한개 이상의 블로그 게시물에 첨부되어 있습니다. 만일 이 파일을 삭제하면 이들 게시물에서도 제거될 것입니다.';
$string['unzipprogress'] = '%s 파일/폴더가 만들어짐';
$string['upload'] = '올리기';
$string['uploadagreement'] = '올리기 동의';
$string['uploadagreementdescription'] = '사용자들이 사이트에 파일을 올리기전에 사용자들로 하여금 아래 문장에 동의하도록 하려면 이 옵션을 활성화 하세요.';
$string['uploadedprofileiconsuccessfully'] = '새로운 개인정보 아이콘을 성공적으로 올리기 했습니다.';
$string['uploadexceedsquota'] = '이 파일을 올리면 디스크 할당량이 초과됩니다. 올린 파일 몇개를 삭제하십시요.';
$string['uploadfile'] = '파일 올리기';
$string['uploadfileexistsoverwritecancel'] = '같은 이름의 파일이 이미 존재합니다.  올리려고 하는 파일의 이름을 변경하거나 기존 파일을 덮어쓰기 할 수 있습니다.';
$string['uploadingfile'] = '파일 올리는 중..';
$string['uploadingfiletofolder'] = '%s를 %s에 올리기';
$string['uploadoffilecomplete'] = '%s 올리기 완료';
$string['uploadoffilefailed'] = '%s 올리기 실패';
$string['uploadoffiletofoldercomplete'] = '%s를 %s에 올리기 완료';
$string['uploadoffiletofolderfailed'] = '%s를 %s에 올리기 실패';
$string['uploadprofileicon'] = '개인정보 아이콘 올리기';
$string['usecustomagreement'] = '맞춤 동의 사용';
$string['usenodefault'] = '기본 아닌것 사용';
$string['usingnodefaultprofileicon'] = '기본이 아닌 개인정보 아이콘 사용';
$string['wav'] = 'WAV 오디오 파일';
$string['wmv'] = 'WMV 비디오 파일';
$string['wrongfiletypeforblock'] = '올린 파일는 이 블록에 맞는 형식이 아닙니다.';
$string['xml'] = 'XML 파일';
$string['youmustagreetothecopyrightnotice'] = '저작권 알림에 동의해야 합니다';
$string['zip'] = 'ZIP 아카이브';
?>
