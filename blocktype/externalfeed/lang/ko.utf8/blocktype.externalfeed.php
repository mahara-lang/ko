<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = '공백으로 남겨 두면 피드의 제목이 사용됩니다.';
$string['description'] = '외부 RSS나 Atom 피드 엠베드';
$string['feedlocation'] = '피드 위치';
$string['feedlocationdesc'] = '유효한 RSS나 Atom 피드 URL';
$string['invalidfeed'] = '피드가 유효하지 않은 것 같습니다. 보고된 오류: %s';
$string['invalidurl'] = 'URL이 유효하지 않습니다. http나 https URL의 피드만 볼 수 있습니다.';
$string['lastupdatedon'] = '%s에 마지막 갱신됨';
$string['showfeeditemsinfull'] = '피드항목 전체를 보여줄까요?';
$string['showfeeditemsinfulldesc'] = '피드항목 요약 혹은 각 피드에 대한 설명도 보여줄 것인가';
$string['title'] = '외부 피드';
?>
