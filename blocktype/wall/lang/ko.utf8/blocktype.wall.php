<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Post'] = '게시글';
$string['backtoprofile'] = '개인정보로 돌아가기';
$string['delete'] = '게시글 삭제';
$string['deletepost'] = '게시글 삭제';
$string['deletepostsuccess'] = '게시글이 성공적으로 삭제되었습니다.';
$string['deletepostsure'] = '하고자 하는 것이 확실합니까? 되돌릴 수 없습니다.';
$string['description'] = '사람들이 댓글을 남길수 있는 영역 표시';
$string['makeyourpostprivate'] = '게시글을 비공개로 할까요?';
$string['maxcharacters'] = '게시글당 최대 %s 글자';
$string['noposts'] = '표시할 벽 게시글이 없음';
$string['otherusertitle'] = '%s의 벽';
$string['postsizelimit'] = '게시글 크기 한계';
$string['postsizelimitdescription'] = '여기서 벽 게시글의 크기를 제한할 수 있습니다. 기존의 게시글은 변경되지 않습니다.';
$string['postsizelimitinvalid'] = '잘못된 수 입니다.';
$string['postsizelimitmaxcharacters'] = '최대 글자수';
$string['postsizelimittoosmall'] = '이 한계는 0보다 커야 합니다.';
$string['reply'] = '답글';
$string['sorrymaxcharacters'] = '죄송합니다. 게시글은 %s 글자이상되어서는 안됩니다.';
$string['title'] = '벽';
$string['viewwall'] = '벽 보기';
$string['wall'] = '벽';
$string['wholewall'] = '전체 벽 보기';
?>
