<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/ko.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['alttext'] = '크리에티브 커몬스 라이선스';
$string['blockcontent'] = '블록 콘텐츠';
$string['by'] = '속성';
$string['by-nc'] = '속성-비상업용';
$string['by-nc-nd'] = '속성-비상업용-비파생물';
$string['by-nc-sa'] = '속성-비상업용-공유';
$string['by-nd'] = '속성-비파생물';
$string['by-sa'] = '속성-공유';
$string['config:noderivatives'] = '당신의 작품을 변경해도 됩니까?';
$string['config:noncommercial'] = '당신의 작품을 상업적으로 사용해도 됩니까?';
$string['config:sharealike'] = '예. 다른 사람이 같이 공유한다면.';
$string['description'] = '보여주기에 크리에이티브 커몬 라이선스 추가';
$string['licensestatement'] = '이 일은<a rel="license" href="%s">Creative Commons %s 3.0 Unported License</a>에 라이선스 되었습니다.';
$string['sealalttext'] = '이 라이선스는 자유 문화 작품에 허용됩니다.';
$string['title'] = '크리에이티브 커몬 라이선스';
?>
